function ShannonFunc(obj,timestep,hs_counter)

y0 = [obj.initialCond(:,end)];

options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','on'); 
[t,y] = ode15s(@f,[obj.time-timestep,obj.time],y0,options,obj,hs_counter,0);

%update the system
obj.initialCond_past = y(end,:)';
% obj.CaT = y(end,39)';
% obj.Vmopast = y(end,1)./obj.segments';


function output = f(t,y,obj,hs_counter,runType)
t

ydot = zeros(size(y));

%% input values
Vmo             = y(1);

mo                 = y(2);
ho                 = y(3);
jo                 = y(4);
do                 = y(5);
fo                 = y(6);
fcaBjo             = y(7);
fcaBslo            = y(8);
xtoso              = y(9);
ytoso              = y(10);
xtofo              = y(11);
ytofo              = y(12);
xkro               = y(13);
xkso               = y(14);
RyRro              = y(15);
RyRoo              = y(16);
RyRio              = y(17);
NaBjo              = y(18);
NaBslo             = y(19);
TnClo              = y(20);
TnCHco             = y(21);
TnCHmo             = y(22);
CaMo               = y(23);
Myoco              = y(24);
Myomo              = y(25);
SRBo               = y(26);
SLLjo              = y(27);
SLLslo             = y(28);
SLHjo              = y(29);
SLHslo             = y(30);
Csqnbo             = y(31);
Ca_sro             = y(32);
Najo               = y(33);
Naslo              = y(34);
Naio               = y(35);
Kio                = y(36);
Cajo               = y(37);
Caslo              = y(38);
Caio               = y(39);
rtoso              = y(40);

%%
% Nernst Potentials
obj.parameters.ena_junc = (1./obj.parameters.FoRT).*log(obj.parameters.Nao./Najo);     % [mV]
obj.parameters.ena_sl = (1./obj.parameters.FoRT).*log(obj.parameters.Nao./Naslo);       % [mV]
obj.parameters.ek = (1./obj.parameters.FoRT).*log(obj.parameters.Ko./Kio);	        % [mV]
obj.parameters.eca_junc = (1./obj.parameters.FoRT./2).*log(obj.parameters.Cao./Cajo);   % [mV]
obj.parameters.eca_sl = (1./obj.parameters.FoRT./2).*log(obj.parameters.Cao./Caslo);     % [mV]
obj.parameters.ecl = (1./obj.parameters.FoRT).*log(obj.parameters.Cli./obj.parameters.Clo);            % [mV]


%% Membrane Currents
% I_Na: Fast Na Current
am = 0.32.*(Vmo+47.13)./(1-exp(-0.1.*(Vmo+47.13)));
bm = 0.08.*exp(-Vmo./11);
if Vmo >= -40
    ah = 0; aj = 0;
    bh = 1./(0.13.*(1+exp(-(Vmo+10.66)./11.1)));
    bj = 0.3.*exp(-2.535e-7.*Vmo)./(1+exp(-0.1.*(Vmo+32)));
else
    ah = 0.135.*exp((80+Vmo)./-6.8);
    bh = 3.56.*exp(0.079.*Vmo)+3.1e5.*exp(0.35.*Vmo);
    aj = (-1.2714e5.*exp(0.2444.*Vmo)-3.474e-5.*exp(-0.04391.*Vmo)).*(Vmo+37.78)./(1+exp(0.311.*(Vmo+79.23)));
    bj = 0.1212.*exp(-0.01052.*Vmo)./(1+exp(-0.1378.*(Vmo+40.14)));
end
dmo = am.*(1-mo)-bm.*mo;
dho = ah.*(1-ho)-bh.*ho;
djo = aj.*(1-jo)-bj.*jo;


I_Na_junc = obj.parameters.Fjunc.*obj.parameters.GNa.*mo.^3.*ho.*jo.*(Vmo-obj.parameters.ena_junc);
I_Na_sl = obj.parameters.Fsl.*obj.parameters.GNa.*mo.^3.*ho.*jo.*(Vmo-obj.parameters.ena_sl);

% I_Na_junc= I_Na_junc1.*(1-flag)+I_Na_junc2.*flag;
% I_Na_sl= I_Na_sl1.*(1-flag)+I_Na_sl2.*flag;
I_Na = I_Na_junc+I_Na_sl;



% I_nabk: Na Background Current
I_nabk_junc = obj.parameters.Fjunc.*obj.parameters.GNaB.*(Vmo-obj.parameters.ena_junc);
I_nabk_sl = obj.parameters.Fsl.*obj.parameters.GNaB.*(Vmo-obj.parameters.ena_sl);
I_nabk = I_nabk_junc+I_nabk_sl;

% I_nak: Na./K Pump Current
sigma = (exp(obj.parameters.Nao./67.3)-1)./7;
fnak = 1./(1+0.1245.*exp(-0.1.*Vmo.*obj.parameters.FoRT)+0.0365.*sigma.*exp(-Vmo.*obj.parameters.FoRT));
I_nak_junc = obj.parameters.Fjunc.*obj.parameters.IbarNaK.*fnak.*obj.parameters.Ko ./(1+(obj.parameters.KmNaip./Najo).^4) ./(obj.parameters.Ko+obj.parameters.KmKo);
I_nak_sl = obj.parameters.Fsl.*obj.parameters.IbarNaK.*fnak.*obj.parameters.Ko ./(1+(obj.parameters.KmNaip./Naslo).^4) ./(obj.parameters.Ko+obj.parameters.KmKo);
I_nak = I_nak_junc+I_nak_sl;

% I_kr: Rapidly Activating K Current
gkr = 0.03.*sqrt(obj.parameters.Ko./5.4);
xrss = 1./(1+exp(-(Vmo+50)./7.5));
tauxr = 1./(1.38e-3.*(Vmo+7)./(1-exp(-0.123.*(Vmo+7)))+6.1e-4.*(Vmo+10)./(exp(0.145.*(Vmo+10))-1));
dxkro = (xrss-xkro)./tauxr;
rkr = 1./(1+exp((Vmo+33)./22.4));
I_kr = gkr.*xkro.*rkr.*(Vmo-obj.parameters.ek);

% I_obj.parameters.ks: Slowly Activating K Current
obj.parameters.pCaks_junc = -log10(Cajo)+3.0; 
obj.parameters.pCaks_sl = -log10(Caslo)+3.0;  
gks_junc = 0.07.*(0.057 +0.19./(1+ exp((-7.2+obj.parameters.pCaks_junc)./0.6)));
gks_sl = 0.07.*(0.057 +0.19./(1+ exp((-7.2+obj.parameters.pCaks_sl)./0.6))); 
eks = (1./obj.parameters.FoRT).*log((obj.parameters.Ko+obj.parameters.pNaK.*obj.parameters.Nao)./(Kio+obj.parameters.pNaK.*Naio));	
xsss = 1./(1+exp(-(Vmo-1.5)./16.7));
tauxs = 1./(7.19e-5.*(Vmo+30)./(1-exp(-0.148.*(Vmo+30)))+1.31e-4.*(Vmo+30)./(exp(0.0687.*(Vmo+30))-1)); 
dxkso = (xsss-xkso)./tauxs;
I_ks_junc = obj.parameters.Fjunc.*gks_junc.*xkso.^2.*(Vmo-eks);
I_ks_sl = obj.parameters.Fsl.*gks_sl.*xkso.^2.*(Vmo-eks);
I_ks = I_ks_junc+I_ks_sl;

%I_kp: Plateau K current
kp_kp = 1./(1+exp(7.488-Vmo./5.98));
I_kp_junc = obj.parameters.Fjunc.*obj.parameters.gkp.*kp_kp.*(Vmo-obj.parameters.ek);
I_kp_sl = obj.parameters.Fsl.*obj.parameters.gkp.*kp_kp.*(Vmo-obj.parameters.ek);
I_kp = I_kp_junc+I_kp_sl;

%% I_to: Transient Outward K Current (slow and fast components)
xtoss = 1./(1+exp(-(Vmo+3.0)./15));
ytoss = 1./(1+exp((Vmo+33.5)./10));
rtoss = 1./(1+exp((Vmo+33.5)./10));
tauxtos = 9./(1+exp((Vmo+3.0)./15))+0.5;
tauytos = 3e3./(1+exp((Vmo+60.0)./10))+30;
%tauytos = 182./(1+exp((Vmo+33.5)./10))+1;
taurtos = 2.8e3./(1+exp((Vmo+60.0)./10))+220; %Fei changed here!! time-dependent gating variable
%taurtos =8085./(1+exp((Vmo+33.5)./10))+313;
dxtoso = (xtoss-xtoso)./tauxtos;
dytoso = (ytoss-ytoso)./tauytos;
drtoso= (rtoss-rtoso)./taurtos; %Fei changed here!! time-dependent gating variable
I_tos = obj.parameters.GtoSlow.*xtoso.*(ytoso+0.5.*rtoso).*(Vmo-obj.parameters.ek);    % [uA./uF]

tauxtof = 3.5.*exp(-Vmo.*Vmo./30./30)+1.5;
%tauxtof = 3.5.*exp(-((Vmo+3)./30).^2)+1.5;
tauytof = 20.0./(1+exp((Vmo+33.5)./10))+20.0;
%tauytof = 20.0./(1+exp((Vmo+33.5)./10))+20.0;
dxtofo = (xtoss-xtofo)./tauxtof;
dytofo = (ytoss-ytofo)./tauytof;
I_tof = obj.parameters.GtoFast.*xtofo.*ytofo.*(Vmo-obj.parameters.ek);
I_to = I_tos + I_tof;

% I_ki: Time-Independent K Current
aki = 1.02./(1+exp(0.2385.*(Vmo-obj.parameters.ek-59.215)));
bki =(0.49124.*exp(0.08032.*(Vmo+5.476-obj.parameters.ek)) + exp(0.06175.*(Vmo-obj.parameters.ek-594.31))) ./(1 + exp(-0.5143.*(Vmo-obj.parameters.ek+4.753)));
kiss = aki./(aki+bki);
I_ki = 0.9.*sqrt(obj.parameters.Ko./5.4).*kiss.*(Vmo-obj.parameters.ek);

% I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current
I_ClCa_junc = obj.parameters.Fjunc.*obj.parameters.GClCa./(1+obj.parameters.KdClCa./Cajo).*(Vmo-obj.parameters.ecl);
I_ClCa_sl = obj.parameters.Fsl.*obj.parameters.GClCa./(1+obj.parameters.KdClCa./Caslo).*(Vmo-obj.parameters.ecl);
I_ClCa = I_ClCa_junc+I_ClCa_sl;
I_Clbk = obj.parameters.GClB.*(Vmo-obj.parameters.ecl);

%% I_Ca: L-type Calcium Current
dss = 1./(1+exp(-(Vmo+14.5)./6.0));
taud = dss.*(1-exp(-(Vmo+14.5)./6.0))./(0.035.*(Vmo+14.5));
fss = 1./(1+exp((Vmo+35.06)./3.6))+0.6./(1+exp((50-Vmo)./20));
tauf = 1./(0.0197.*exp( -(0.0337.*(Vmo+14.5)).^2 )+0.02);
ddo = (dss-do)./taud;
dfo = (fss-fo)./tauf;
dfcaBjo = 1.7.*Cajo.*(1-fcaBjo)-11.9e-3.*fcaBjo; % fCa_junc   obj.parameters.Koff!!!!!!!!
dfcaBslo = 1.7.*Caslo.*(1-fcaBslo)-11.9e-3.*fcaBslo; % fCa_sl
fcaCaMSL= 0.1./(1+(0.01./Caslo));
fcaCaj= 0.1./(1+(0.01./Cajo));
fcaCaMSL=0;
fcaCaj= 0;
%fcaBjo=0;
%fcaBslo=0;
ibarca_j = obj.parameters.pCa.*4.*(Vmo.*obj.parameters.Frdy.*obj.parameters.FoRT) .* (0.341.*Cajo.*exp(2.*Vmo.*obj.parameters.FoRT)-0.341.*obj.parameters.Cao) ./(exp(2.*Vmo.*obj.parameters.FoRT)-1);
ibarca_sl = obj.parameters.pCa.*4.*(Vmo.*obj.parameters.Frdy.*obj.parameters.FoRT) .* (0.341.*Caslo.*exp(2.*Vmo.*obj.parameters.FoRT)-0.341.*obj.parameters.Cao) ./(exp(2.*Vmo.*obj.parameters.FoRT)-1);
ibark = obj.parameters.pK.*(Vmo.*obj.parameters.Frdy.*obj.parameters.FoRT).*(0.75.*Kio.*exp(Vmo.*obj.parameters.FoRT)-0.75.*obj.parameters.Ko) ./(exp(Vmo.*obj.parameters.FoRT)-1);
ibarna_j = obj.parameters.pNa.*(Vmo.*obj.parameters.Frdy.*obj.parameters.FoRT) .*(0.75.*Najo.*exp(Vmo.*obj.parameters.FoRT)-0.75.*obj.parameters.Nao)  ./(exp(Vmo.*obj.parameters.FoRT)-1);
ibarna_sl = obj.parameters.pNa.*(Vmo.*obj.parameters.Frdy.*obj.parameters.FoRT) .*(0.75.*Naslo.*exp(Vmo.*obj.parameters.FoRT)-0.75.*obj.parameters.Nao)  ./(exp(Vmo.*obj.parameters.FoRT)-1);
I_Ca_junc = (obj.parameters.Fjunc_CaL.*ibarca_j.*do.*fo.*((1-fcaBjo)+fcaCaj).*obj.parameters.Q10CaL.^obj.parameters.Qpow).*0.45.*1;
I_Ca_sl = (obj.parameters.Fsl_CaL.*ibarca_sl.*do.*fo.*((1-fcaBslo)+fcaCaMSL).*obj.parameters.Q10CaL.^obj.parameters.Qpow).*0.45.*1;
I_Ca = I_Ca_junc+I_Ca_sl;
I_CaK = (ibark.*do.*fo.*(obj.parameters.Fjunc_CaL.*(fcaCaj+(1-fcaBjo))+obj.parameters.Fsl_CaL.*(fcaCaMSL+(1-fcaBslo))).*obj.parameters.Q10CaL.^obj.parameters.Qpow).*0.45.*1;
I_CaNa_junc = (obj.parameters.Fjunc_CaL.*ibarna_j.*do.*fo.*((1-fcaBjo)+fcaCaj).*obj.parameters.Q10CaL.^obj.parameters.Qpow).*0.45.*1;
I_CaNa_sl = (obj.parameters.Fsl_CaL.*ibarna_sl.*do.*fo.*((1-fcaBslo)+fcaCaMSL).*obj.parameters.Q10CaL.^obj.parameters.Qpow).*.45.*1;
I_CaNa = I_CaNa_junc+I_CaNa_sl;
I_Catot = I_Ca+I_CaK+I_CaNa;

% I_ncx: Na./Ca Exchanger flux
Ka_junc = 1./(1+(obj.parameters.Kdact./Cajo).^3);
Ka_sl = 1./(1+(obj.parameters.Kdact./Caslo).^3);
s1_junc = exp(obj.parameters.nu.*Vmo.*obj.parameters.FoRT).*Najo.^3.*obj.parameters.Cao;
s1_sl = exp(obj.parameters.nu.*Vmo.*obj.parameters.FoRT).*Naslo.^3.*obj.parameters.Cao;
s2_junc = exp((obj.parameters.nu-1).*Vmo.*obj.parameters.FoRT).*obj.parameters.Nao.^3.*Cajo;
s3_junc = obj.parameters.KmCai.*obj.parameters.Nao.^3.*(1+(Najo./obj.parameters.KmNai).^3) + obj.parameters.KmNao.^3.*Cajo.*(1+Cajo./obj.parameters.KmCai)+obj.parameters.KmCao.*Najo.^3+Najo.^3.*obj.parameters.Cao+obj.parameters.Nao.^3.*Cajo;
s2_sl = exp((obj.parameters.nu-1).*Vmo.*obj.parameters.FoRT).*obj.parameters.Nao.^3.*Caslo;
s3_sl = obj.parameters.KmCai.*obj.parameters.Nao.^3.*(1+(Naslo./obj.parameters.KmNai).^3) + obj.parameters.KmNao.^3.*Caslo.*(1+Caslo./obj.parameters.KmCai)+obj.parameters.KmCao.*Naslo.^3+Naslo.^3.*obj.parameters.Cao+obj.parameters.Nao.^3.*Caslo;
I_ncx_junc = obj.parameters.Fjunc.*obj.parameters.IbarNCX.*obj.parameters.Q10NCX.^obj.parameters.Qpow.*Ka_junc.*(s1_junc-s2_junc)./s3_junc./(1+obj.parameters.ksat.*exp((obj.parameters.nu-1).*Vmo.*obj.parameters.FoRT));
I_ncx_sl = obj.parameters.Fsl.*obj.parameters.IbarNCX.*obj.parameters.Q10NCX.^obj.parameters.Qpow.*Ka_sl.*(s1_sl-s2_sl)./s3_sl./(1+obj.parameters.ksat.*exp((obj.parameters.nu-1).*Vmo.*obj.parameters.FoRT));
I_ncx = I_ncx_junc+I_ncx_sl;

% I_obj.parameters.pCa: Sarcolemmal Ca Pump Current
I_pCa_junc = obj.parameters.Fjunc.*obj.parameters.Q10SLCaP.^obj.parameters.Qpow.*obj.parameters.IbarSLCaP.*Cajo.^1.6./(obj.parameters.KmPCa.^1.6+Cajo.^1.6);
I_pCa_sl = obj.parameters.Fsl.*obj.parameters.Q10SLCaP.^obj.parameters.Qpow.*obj.parameters.IbarSLCaP.*Caslo.^1.6./(obj.parameters.KmPCa.^1.6+Caslo.^1.6);
I_pCa = I_pCa_junc+I_pCa_sl;

% I_cabk: Ca Background Current
I_cabk_junc = obj.parameters.Fjunc.*obj.parameters.GCaB.*(Vmo-obj.parameters.eca_junc);
I_cabk_sl = obj.parameters.Fsl.*obj.parameters.GCaB.*(Vmo-obj.parameters.eca_sl);
I_cabk = I_cabk_junc+I_cabk_sl;

%% SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
MaxSR = 15; MinSR = 1;
kCaSR = MaxSR - (MaxSR-MinSR)./(1+(obj.parameters.ec50SR./Ca_sro).^2.5);
obj.parameters.KoSRCa = obj.parameters.koCa./kCaSR;
kiSRCa = obj.parameters.kiCa.*kCaSR;
RI = 1-RyRro-RyRoo-RyRio;
dRyRro = (obj.parameters.kim.*RI-kiSRCa.*Cajo.*RyRro)-(obj.parameters.KoSRCa.*Cajo.^2.*RyRro-obj.parameters.kom.*RyRoo);   % R
dRyRoo = (obj.parameters.KoSRCa.*Cajo.^2.*RyRro-obj.parameters.kom.*RyRoo)-(kiSRCa.*Cajo.*RyRoo-obj.parameters.kim.*RyRio);% O
dRyRio = (kiSRCa.*Cajo.*RyRoo-obj.parameters.kim.*RyRio)-(obj.parameters.kom.*RyRio-obj.parameters.KoSRCa.*Cajo.^2.*RI);   % I


% %% Traveling wave
% if hs_counter == round(obj.segments./2)
%     if t>=2 && t<=2.5
%         dRyRoo = 1;%O
%     end 
% end

%%
J_SRCarel = obj.parameters.ks.*RyRoo.*(Ca_sro-Cajo);          % [mM./ms]

J_serca = obj.parameters.Q10SRCaP.^obj.parameters.Qpow.*obj.parameters.Vmax_SRCaP.*((Caio./obj.parameters.Kmf).^obj.parameters.hillSRCaP-(Ca_sro./obj.parameters.Kmr).^obj.parameters.hillSRCaP)...
    ./(1+(Caio./obj.parameters.Kmf).^obj.parameters.hillSRCaP+(Ca_sro./obj.parameters.Kmr).^obj.parameters.hillSRCaP);
J_SRleak = 5.348e-6.*(Ca_sro-Cajo);           %   [mM./ms]

%% Sodium and Calcium Buffering
dNaBjo = obj.parameters.kon_na.*Najo.*(obj.parameters.Bmax_Naj-NaBjo)-obj.parameters.koff_na.*NaBjo;        % NaBj      [mM./ms]
dNaBslo = obj.parameters.kon_na.*Naslo.*(obj.parameters.Bmax_Nasl-NaBslo)-obj.parameters.koff_na.*NaBslo;       % NaBsl     [mM./ms]

% Cytosolic Ca Buffers
dTnClo = obj.parameters.kon_tncl.*Caio.*(obj.parameters.Bmax_TnClow-TnClo)-obj.parameters.koff_tncl.*TnClo;            % TnCL      [mM./ms]
% dTnClo = obj.parameters.Bmax_TnClow.*obj.TnC;
dTnCHco = obj.parameters.kon_tnchca.*Caio.*(obj.parameters.Bmax_TnChigh-TnCHco-TnCHmo)-obj.parameters.koff_tnchca.*TnCHco; % TnCHc     [mM./ms]
% dTnCHco = obj.parameters.Bmax_TnChigh .* [obj.TnC,obj.TnC];
dTnCHmo = obj.parameters.kon_tnchmg.*obj.parameters.Mgi.*(obj.parameters.Bmax_TnChigh-TnCHco-TnCHmo)-obj.parameters.koff_tnchmg.*TnCHmo;   % TnCHm     [mM./ms]
dCaMo = obj.parameters.kon_cam.*Caio.*(obj.parameters.Bmax_TnChigh-CaMo)-obj.parameters.koff_cam.*CaMo;                 % CaM       [mM./ms]
dMyoco = obj.parameters.kon_myoca.*Caio.*(obj.parameters.Bmax_myosin-Myoco-Myomo)-obj.parameters.koff_myoca.*Myoco;    % Myosin_ca [mM./ms]
dMyomo = obj.parameters.kon_myomg.*obj.parameters.Mgi.*(obj.parameters.Bmax_myosin-Myoco-Myomo)-obj.parameters.koff_myomg.*Myomo;      % Myosin_mg [mM./ms]
dSRBo = obj.parameters.kon_sr.*Caio.*(obj.parameters.Bmax_SR-SRBo)-obj.parameters.koff_sr.*SRBo;                    % SRB       [mM./ms]
J_CaB_cytosol = dTnClo+dTnCHco+dTnCHmo+dCaMo+dMyoco+dMyomo+dSRBo;

% Junctional and SL Ca Buffers
dSLLjo = obj.parameters.kon_sll.*Cajo.*(obj.parameters.Bmax_SLlowj-SLLjo)-obj.parameters.koff_sll.*SLLjo;       % SLLj      [mM./ms]
dSLLslo = obj.parameters.kon_sll.*Caslo.*(obj.parameters.Bmax_SLlowsl-SLLslo)-obj.parameters.koff_sll.*SLLslo;      % SLLsl     [mM./ms]
dSLHjo = obj.parameters.kon_slh.*Cajo.*(obj.parameters.Bmax_SLhighj-SLHjo)-obj.parameters.koff_slh.*SLHjo;      % SLHj      [mM./ms]
dSLHslo = obj.parameters.kon_slh.*Caslo.*(obj.parameters.Bmax_SLhighsl-SLHslo)-obj.parameters.koff_slh.*SLHslo;     % SLHsl     [mM./ms]
J_CaB_junction = dSLLjo+dSLHjo;
J_CaB_sl = dSLLslo+dSLHslo;

%% Ion concentrations
% SR Ca Concentrations
dCsqnbo = obj.parameters.kon_csqn.*Ca_sro.*(obj.parameters.Bmax_Csqn-Csqnbo)-obj.parameters.koff_csqn.*Csqnbo;       % Csqn      [mM./ms]
% dCa_sro = J_serca-(J_SRleak.*Vmyo./obj.parameters.Vsr+J_SRCarel)-dCsqnbo;         % Ca_sr     [mM./ms] %Ratio 3 leak current

% Sodium Concentrations
I_Na_tot_junc = I_Na_junc+I_nabk_junc+3.*I_ncx_junc+3.*I_nak_junc+I_CaNa_junc;   % [uA./uF]
I_Na_tot_sl = I_Na_sl+I_nabk_sl+3.*I_ncx_sl+3.*I_nak_sl+I_CaNa_sl;   % [uA./uF]

dNajo = -I_Na_tot_junc.*obj.parameters.Cmem./(obj.parameters.Vjunc.*obj.parameters.Frdy)+obj.parameters.J_na_juncsl./(obj.parameters.Vjunc).*(Naslo-Najo)-dNaBjo;
dNaslo = -I_Na_tot_sl.*obj.parameters.Cmem./(obj.parameters.Vsl.*obj.parameters.Frdy)+obj.parameters.J_na_juncsl./(obj.parameters.Vsl).*(Najo-Naslo)...
   +obj.parameters.J_na_slmyo./(obj.parameters.Vsl).*(Naio-Naslo)-dNaBslo;
%dNajo = 0;
%dNaslo = 0;
dNaio = obj.parameters.J_na_slmyo./(obj.parameters.Vmyo).*(Naslo-Naio);             % [mM./msec] 
%dNajo=0;

% Potassium Concentration
I_K_tot = I_to+I_kr+I_ks+I_ki-2.*I_nak+I_CaK+I_kp;     % [uA./uF]
% dKio = 0; %-I_K_tot.*Cmem./(obj.parameters.Vmyo.*obj.parameters.Frdy);           % [mM./msec]
dKio =0; % -I_K_tot.*Cmem./(obj.parameters.Vmyo.*obj.parameters.Frdy);

% Calcium Concentrations
I_Ca_tot_junc = I_Ca_junc+I_cabk_junc+I_pCa_junc-2.*I_ncx_junc;                   % [uA./uF]
I_Ca_tot_sl = I_Ca_sl+I_cabk_sl+I_pCa_sl-2.*I_ncx_sl;            % [uA./uF]

%% For the first, leftmost 1 (d=1) connected to membrane 1
J_diff_CytSL = obj.parameters.J_ca_slmyo*(Caio-Caslo);
J_diff_juncSL = obj.parameters.J_ca_juncsl.*(Cajo-Caslo);

dCajo = -I_Ca_tot_junc.*obj.parameters.Cmem./(obj.parameters.Vjunc.*2.*obj.parameters.Frdy)...
    - J_CaB_junction+(J_SRCarel).*obj.parameters.Vsr./obj.parameters.Vjunc...
    + J_SRleak.*obj.parameters.Vmyo./obj.parameters.Vjunc...
    - J_diff_juncSL./obj.parameters.Vjunc;

dCaslo = -I_Ca_tot_sl.*obj.parameters.Cmem./(obj.parameters.Vsl.*2.*obj.parameters.Frdy)...
    - J_CaB_sl + J_diff_juncSL./obj.parameters.Vsl + J_diff_CytSL./obj.parameters.Vsl;

dCaio = -J_serca.*obj.parameters.Vsr./obj.parameters.Vmyo...
    - J_CaB_cytosol - J_diff_CytSL./obj.parameters.Vmyo;

dCa_sro = J_serca...
    - (J_SRleak.*obj.parameters.Vmyo./obj.parameters.Vsr+J_SRCarel)...
    - dCsqnbo; 


% %% SAC
% %%
% % I_NS (cation-nonselective SAC) current
% E_ns = -10; %mV
% G_K0 = 1./10; %zwischen 0 und 1./10
% G_ns = 0.08;%0.08;
% I_ns = G_ns .* (Vmo - E_ns);
% 
% %SAC_K (K+ selective SAC) current
% I_K0 = G_K0 ./ (1 + exp((19.05 - Vmo)./29.98));
% 
% %SAC current
% I_SAC = (I_ns + I_K0) .* (strainfunc);
% 
% 
% I_SAC = [I_SAC,I_SAC];
% I_SAC = zeros(size([strainfunc,strainfunc]));
% % if (nargin == 6) & strcmp(runType,'currents')
% %     I_SAC = [I_SAC(1:segments);I_SAC(segments+1:end)]';
% % end

I_SAC = 0;
%% Simulation type
protocol = 'pace1';

switch lower(protocol)
    case {'none',''},
        I_app = 0;
    case 'pace1',        % pace w/ current injection at rate 'rate'
		rate = 1e-3;
		if mod(t+995,1/rate) <= 5
            I_app = 9.5;
        else
            I_app = 0.0;
        end
%          I_app = 0.0;
    case 'pace2',        % pace w/ current injection at rate 'rate'
		factor = 2;
        rate = factor*1e-3;
		if (mod(t+900,1/rate) <= 5) & ((t > 5000) & (t < 10000))
            I_app = 12.0;
        elseif (mod(t+900,1/rate*factor*2) <= 5)  & ((t <= 5000) | (t >= 10000))
            I_app = 12.0;
        else
            I_app = 0;
        end    
        I_app = 0;
    case 'pace3',
        rate = 40e-3;
        if (t > 1000) & (t < 2000) & (mod(t+900,1/rate) <= 5) 
                    I_app = 10;    
        elseif (t > 2000) & (mod(t+900,1/rate*10) <= 5)
                    I_app = 10;
                    else
            I_app = 0;
        end
    case 'vclamp',      
		V_hold = -90;
        V_test = 0;
		if (t > 100 & t < 300)
		    V_clamp = V_test;
		else
		    V_clamp = V_hold;
		end
		R_clamp = 0.02;
		I_app = (V_clamp-Vmo)/R_clamp;
    case 'fig5',
        rate = 0.5e-3;
        %if t<=60000
         %   V_clamp=-90;
        %else
        if mod(t,1/rate)<=200
            V_clamp = -20;
		else
		    V_clamp = -90;
        end
        %end
  
    R_clamp = 0.02;
		I_app = (V_clamp-Vmo)/R_clamp;
            
end  

%% Membrane Potential
I_Na_tot = I_Na_tot_junc + I_Na_tot_sl;          % [uA./uF]
I_Cl_tot = I_ClCa+I_Clbk;                        % [uA./uF]
I_Ca_tot = I_Ca_tot_junc+I_Ca_tot_sl;
I_tot = I_Na_tot+I_Cl_tot+I_Ca_tot+I_K_tot+I_SAC;
%dVmo = -(I_Ca_tot+I_K_tot+I_Na_tot-I_app);
dVmo = -(I_tot-I_app);
vmax = dVmo;

%% output vector
ydot(1)  =  dVmo;

%membrane 1
ydot(2)  =  dmo;
ydot(3)  =  dho;
ydot(4)  =  djo;
ydot(5)  =  ddo;
ydot(6)  =  dfo;
ydot(7)  =  dfcaBjo;
ydot(8)  =  dfcaBslo;
ydot(9)  =  dxtoso;
ydot(10)  =  dytoso;
ydot(11)  =  dxtofo;
ydot(12)  =  dytofo;
ydot(13)  =  dxkro;
ydot(14)  =  dxkso;
ydot(15)  =  dRyRro;
ydot(16)  =  dRyRoo;
ydot(17)  =  dRyRio;
ydot(18)  =  dNaBjo;
ydot(19)  =  dNaBslo;
ydot(20)  =  dTnClo;
ydot(21)  =  dTnCHco;
ydot(22)  =  dTnCHmo;
ydot(23)  =  dCaMo;
ydot(24)  =  dMyoco;
ydot(25)  =  dMyomo;
ydot(26)  =  dSRBo;
ydot(27)  =  dSLLjo;
ydot(28)  =  dSLLslo;
ydot(29)  =  dSLHjo;
ydot(30)  =  dSLHslo;
ydot(31)  =  dCsqnbo;
ydot(32)  =  dCa_sro;
ydot(33)  =  dNajo;
ydot(34)  =  dNaslo;
ydot(35)  =  dNaio;
ydot(36)  =  dKio;
ydot(37)  =  dCajo;
ydot(38)  =  dCaslo;
ydot(39)  =  dCaio;
ydot(40)  =  drtoso;


if isreal(y) == 0
    disp('halt');
end

if isreal(ydot) == 0
    disp('halt');
end

% ----- END EC COUPLING MODEL ---------------
% adjust output depending on the function call

if (nargin == 5)
    output = ydot;
elseif (nargin == 6) & strcmp(runType,'ydot')
    output = ydot;
elseif (nargin == 6) & strcmp(runType,'rates')
    output = r;
elseif (nargin == 6) & strcmp(runType,'currents')
    %currents = [I_Na I_nabk I_nak I_kr I_obj.parameters.ks I_kp I_tos I_tof I_ki I_ClCa I_Clbk I_Catot I_ncx I_obj.parameters.pCa I_cabk J_serca.*obj.parameters.Vmyo./obj.parameters.Vsr];
    %currents = [I_Na I_tof I_tos I_kr I_obj.parameters.ks I_ClCa I_Catot J_SRCarel.*obj.parameters.Vsr./obj.parameters.Vmyo J_SRleak RI I_ncx]; 
    currents= [I_ncx' I_ncx(:,2)'];
    output = currents;
end
