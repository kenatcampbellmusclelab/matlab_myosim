function implement_time_step(obj,time_step,no_of_half_sarcomeres,segment)

% Update segments
obj.segments = no_of_half_sarcomeres;

%update time
obj.time = obj.time + time_step;

obj.ShannonFunc(time_step,segment);