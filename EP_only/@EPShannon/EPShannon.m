classdef EPShannon < handle
    
    properties
        % These are properties that can be accessed from outside the
        % EP class
        
        TnC = 8.773191e-3;
        time = 0;
        initialCond;
        Vmo;
    end
    
    properties (SetAccess = private)
        % These are properties that can only be accessed from within the
        % EP class
        
        parameters;
        initialCond_past;
        Vmopast;
        
        segments = Electrophysiology.segmentsCounter;
    end
    
    methods
        
        % Constructor
        function obj = EPShannon(varargin)
            % EP Model Parameters
            obj.parameters.R = 8314;       % [J./kmol.*K]
            obj.parameters.Frdy = 96485;   % [C./mol]
            obj.parameters.Temp = 310;     % [K]
            obj.parameters.FoRT = obj.parameters.Frdy./obj.parameters.R./obj.parameters.Temp;
            obj.parameters.Cmem = 1.3810e-10./obj.segments;   % [F] membrane capacitance
            obj.parameters.Qpow = (obj.parameters.Temp-310)./10;
            
            % Cell geometry
            obj.parameters.cellLength = 100./obj.segments;     % cell length [um]
            obj.parameters.cellRadius = 10.25;   % cell radius [um]
            obj.parameters.Vcell = pi.*obj.parameters.cellRadius.^2.*obj.parameters.cellLength.*1e-15;    % [L]
            obj.parameters.Vmyo = 0.65.*obj.parameters.Vcell;
            obj.parameters.Vsr = 0.035.*obj.parameters.Vcell;
            obj.parameters.Vsl = 0.02.*obj.parameters.Vcell;
            obj.parameters.Vjunc = 0.0539.*.01.*obj.parameters.Vcell;
            
            obj.parameters.J_ca_juncsl = 1./1.2134e12./obj.segments; % [L./msec] = 8.2413e-13 
            obj.parameters.J_ca_slmyo = 1./2.68510e11./obj.segments; % [L./msec] = 3.2743e-12
            obj.parameters.J_na_juncsl = 1./(1.6382e12./3.*100)./obj.segments; % [L./msec] = 6.1043e-13
            obj.parameters.J_na_slmyo = 1./(1.8308e10./3.*100)./obj.segments;  % [L./msec] = 5.4621e-11
 
            
            % Fractional currents in compartments
            obj.parameters.Fjunc = 0.11;
            obj.parameters.Fsl = 1-obj.parameters.Fjunc;
            obj.parameters.Fjunc_CaL = 0.9;
            obj.parameters.Fsl_CaL = 1-obj.parameters.Fjunc_CaL;
            
            % Fixed ion concentrations
            obj.parameters.Cli = 15;   % Intracellular Cl  [mM]
            obj.parameters.Clo = 150;  % Extracellular Cl  [mM]
            obj.parameters.Ko = 5.4;   % Extracellular K   [mM]
            obj.parameters.Nao = 140;  % Extracellular Na  [mM]
            obj.parameters.Cao = 1.8;  % Extracellular Ca  [mM]
            obj.parameters.Mgi = 1;    % Intracellular Mg  [mM]
            
            % Na transport parameters
            
            obj.parameters.GNa=16;
            obj.parameters.GNaB = 0.297e-3;    % [mS./uF]
            obj.parameters.IbarNaK = 1.90719;     % [uA./uF]
            obj.parameters.KmNaip = 11;         % [mM]
            obj.parameters.KmKo = 1.5;         % [mM]
            obj.parameters.Q10NaK = 1.63;
            obj.parameters.Q10KmNai = 1.39;
            
            %% K current parameters
            obj.parameters.pNaK = 0.01833;
            obj.parameters.GtoSlow = 0.06.*1;     % [mS./uF] %0.09 CaMKII
            obj.parameters.GtoFast = 0.02.*1;     % [mS./uF]
            obj.parameters.gkp = 0.001;
            
            % Cl current parameters
            obj.parameters.GClCa = 0.109625;   % [mS./uF]
            obj.parameters.GClB = 9e-3;        % [mS./uF]
            obj.parameters.KdClCa = 100e-3;    % [mM]
            
            % I_Ca parameters
            obj.parameters.pNa = 1.5e-8;       % [cm./sec]
            obj.parameters.pCa = 5.4e-4;       % [cm./sec]
            obj.parameters.pK = 2.7e-7;        % [cm./sec]
            obj.parameters.Q10CaL = 1.8;
            
            % Ca transport parameters
            obj.parameters.IbarNCX = 9.0;      % [uA./uF]
            obj.parameters.KmCai = 3.59e-3;    % [mM]
            obj.parameters.KmCao = 1.3;        % [mM]
            obj.parameters.KmNai = 12.29;      % [mM]
            obj.parameters.KmNao = 87.5;       % [mM]
            obj.parameters.ksat = 0.27;        % [none]
            obj.parameters.nu = 0.35;          % [none]
            obj.parameters.Kdact = 0.256e-3;   % [mM]
            obj.parameters.Q10NCX = 1.57;      % [none]
            obj.parameters.IbarSLCaP = 0.0673; % [uA./uF](2.2 umol./L cytosol./sec)
            obj.parameters.KmPCa = 0.5e-3;     % [mM]
            obj.parameters.GCaB = 2.513e-4;    % [uA./uF]
            obj.parameters.Q10SLCaP = 2.35;    % [none]
            
            % SR flux parameters
            obj.parameters.Q10SRCaP = 2.6;          % [none]
            obj.parameters.Vmax_SRCaP = 5.3114e-3;  % [mM./msec] (286 umol./L cytosol./sec)
            obj.parameters.Kmf = 0.246e-3;          % [mM]
            obj.parameters.Kmr = 1.7;               % [mM]L cytosol
            obj.parameters.hillSRCaP = 1.787;       % [mM]
            obj.parameters.ks = 25;                 % [1./ms]
            obj.parameters.koCa = 10;               % [mM.^-2 1./ms]   %default 10   modified 20
            obj.parameters.kom = 0.06;              % [1./ms]
            obj.parameters.kiCa = 0.5;              % [1./mM./ms]
            obj.parameters.kim = 0.005;             % [1./ms]
            obj.parameters.ec50SR = 0.45;           % [mM]
            
            % Buffering parameters
            obj.parameters.Bmax_Naj = 7.561;       % [mM]  % Na buffering
            obj.parameters.Bmax_Nasl = 1.65;       % [mM]
            obj.parameters.koff_na = 1e-3;         % [1./ms]
            obj.parameters.kon_na = 0.1e-3;        % [1./mM./ms]
            obj.parameters.Bmax_TnClow = 70e-3;    % [mM]                      % TnC low affinity
            obj.parameters.koff_tncl = 19.6e-3;    % [1./ms]
            obj.parameters.kon_tncl = 32.7;        % [1./mM./ms]
            obj.parameters.Bmax_TnChigh = 140e-3;  % [mM]                      % TnC high affinity
            obj.parameters.koff_tnchca = 0.032e-3; % [1./ms]
            obj.parameters.kon_tnchca = 2.37;      % [1./mM./ms]
            obj.parameters.koff_tnchmg = 3.33e-3;  % [1./ms]
            obj.parameters.kon_tnchmg = 3e-3;      % [1./mM./ms]
            obj.parameters.Bmax_CaM = 24e-3;       % [mM]  % CaM buffering
            obj.parameters.koff_cam = 238e-3;      % [1./ms]
            obj.parameters.kon_cam = 34;           % [1./mM./ms]
            obj.parameters.Bmax_myosin = 140e-3;   % [mM]                      % Myosin buffering
            obj.parameters.koff_myoca = 0.46e-3;   % [1./ms]
            obj.parameters.kon_myoca = 13.8;       % [1./mM./ms]
            obj.parameters.koff_myomg = 0.057e-3;  % [1./ms]
            obj.parameters.kon_myomg = 0.0157;     % [1./mM./ms]
            obj.parameters.Bmax_SR = 19.*.9e-3;     % [mM] (Bers text says 47e-3) 19e-3
            obj.parameters.koff_sr = 60e-3;        % [1./ms]
            obj.parameters.kon_sr = 100;           % [1./mM./ms]
            obj.parameters.Bmax_SLlowsl = 37.4e-3.*obj.parameters.Vmyo./obj.parameters.Vsl;        % [mM]    % SL buffering
            obj.parameters.Bmax_SLlowj = 4.6e-3.*obj.parameters.Vmyo./obj.parameters.Vjunc.*0.1;    % [mM]
            obj.parameters.koff_sll = 1300e-3;     % [1./ms]
            obj.parameters.kon_sll = 100;          % [1./mM./ms]
            obj.parameters.Bmax_SLhighsl = 13.4e-3.*obj.parameters.Vmyo./obj.parameters.Vsl;       % [mM]
            obj.parameters.Bmax_SLhighj = 1.65e-3.*obj.parameters.Vmyo./obj.parameters.Vjunc.*0.1;  % [mM]
            obj.parameters.koff_slh = 30e-3;       % [1./ms]
            obj.parameters.kon_slh = 100;          % [1./mM./ms]
            obj.parameters.Bmax_Csqn = 140e-3.*obj.parameters.Vmyo./obj.parameters.Vsr;            % [mM] % Bmax_Csqn = 2.6;      % Csqn buffering
            obj.parameters.koff_csqn = 65;         % [1./ms]
            obj.parameters.kon_csqn = 100;         % [1./mM./ms]
            
            
            %% Initial conditions
            mo=1.405627e-3;
            ho= 9.867005e-1;
            jo=9.915620e-1; 
            do=7.175662e-6; 
            fo=1.000681; 
            fcaBjo=2.421991e-2;
            fcaBslo=1.452605e-2;
            xtoso=4.051574e-3;
            ytoso=9.945511e-1; 
            xtofo=4.051574e-3; 
            ytofo= 9.945511e-1; 
            xkro=8.641386e-3; 
            xkso= 5.412034e-3;
            RyRro=8.884332e-1;
            RyRoo=8.156628e-7; 
            RyRio=1.024274e-7; 
            NaBjo=3.539892;
            NaBslo=7.720854e-1; 
            TnClo=8.773191e-3; 
            TnCHco=1.078283e-1; 
            TnCHmo=1.524002e-2; 
            CaMo=2.911916e-4; 
            Myoco=1.298754e-3; 
            Myomo=1.381982e-1;
            SRBo=2.143165e-3; 
            SLLjo=9.566355e-3; 
            SLLslo=1.110363e-1; 
            SLHjo=7.347888e-3; 
            SLHslo=7.297378e-2; 
            Csqnbo= 1.242988;
            Ca_sro=5.545201e-1; %0.9 %% changed
            Najo=8.80329; 
            Naslo=8.80733; 
            Naio=8.80853; 
            Kio=135; 
            Cajo=1.737475e-4; 
            Caslo= 1.031812e-4; 
            Caio=8.597401e-5; 
            Vmo=-8.556885e+1; 
            rtoso=0.9946; 
            ICajuncinto=0; 
            ICaslinto=0;

            %%
            % Gating variables   
            y00 = Vmo;
            %   1       2       3       4       5       6       7       8       9       10      11      12      13
            %%   m       h       j       d       f       fcaBj   fcaBsl   xtos    ytos    xtof    ytof    xkr     xks   
            %y10=[1.2e-3;0.99;   0.99;   0.0;    1.0;    0.0141; 0.0141;     0;      1;      0.0;    1.0;    0.0;    6e-3;];
            y10=[mo; ho; jo; do; fo; fcaBjo; fcaBslo; xtoso; ytoso; xtofo; ytofo; xkro; xkso;];  
            % RyR and Buffering variables
            %   14      15      16      17      18      19      20      21      22      23      24
            %%   RyRr    RyRo    RyRi    NaBj    NaBsl   TnCL    TnCHc   TnCHm   CaM     Myoc    Myom  
            y20=[RyRro; RyRoo; RyRio; NaBjo; NaBslo; TnClo; TnCHco; TnCHmo; CaMo; Myoco; Myomo;];     
            %y20=[1;     0;      0;      1.8;   0.8;    0.012;   0.112;  0.01;   0.4e-3; 1.9e-3; 0.135;];
            % More buffering variables
            %   25      26      27      28      29      30
            %%   SRB     SLLj   SLLsl    SLHj    SLHsl  Csqnb
            y30=[SRBo; SLLjo; SLLslo; SLHjo; SLHslo; Csqnbo];
            %y30=[3.3e-3; 0.012; 0.012; 0.13;  0.13;  1.5;];
            %   Intracellular concentrations./ Membrane voltage
            %    31      32      33      34      35      36      37     38       39    40    
            %%    Ca_sr   Naj     Nasl    Nai     Ki      Caj     Casl    Cai     Vm    rtos  
            y40=[Ca_sro; Najo; Naslo; Naio; Kio; Cajo; Caslo; Caio; rtoso; ];    
            %y40=[0.9;    8.8;    8.8;    8.8;    135;    0.1e-3; 0.1e-3; 0.1e-3; -88;  0.89; 0;          0;];


            %% Put everything together
            obj.initialCond= [y00;y10;y20;y30;y40];
            
            obj.Vmo = Vmo;
            
%             % Nernst Potentials
%             obj.parameters.ena_junc = (1./obj.parameters.FoRT).*log(obj.parameters.Nao./Najo);     % [mV]
%             obj.parameters.ena_sl = (1./obj.parameters.FoRT).*log(obj.parameters.Nao./Naslo);       % [mV]
%             obj.parameters.ek = (1./obj.parameters.FoRT).*log(obj.parameters.Ko./Kio);	        % [mV]
%             obj.parameters.eca_junc = (1./obj.parameters.FoRT./2).*log(obj.parameters.Cao./Cajo);   % [mV]
%             obj.parameters.eca_sl = (1./obj.parameters.FoRT./2).*log(obj.parameters.Cao./Caslo);     % [mV]
%             obj.parameters.ecl = (1./obj.parameters.FoRT).*log(obj.parameters.Cli./obj.parameters.Clo);            % [mV]
            
        end
        % other methods
        ShannonFunc(obj,timestep,hs_counter);
        
        implement_time_step(obj,time_step,segments,hs_counter);
    end
end