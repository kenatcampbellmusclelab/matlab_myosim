function Voigt(obj,time_step)
segments = obj.segmentsCounter;

for NoSegments = 1:segments
    Caio(NoSegments)    = obj.Shannon(NoSegments).initialCond_past(39);
    Caslo(NoSegments)   = obj.Shannon(NoSegments).initialCond_past(38);
    Ca_sro(NoSegments)  = obj.Shannon(NoSegments).initialCond_past(32);
    
    obj.output(:,NoSegments) = obj.Shannon(NoSegments).initialCond_past;
end

tau_seg_cyt = 0.6.*4;%[ms]
tau_seg_SR = 15.*25;%[ms]
tau_seg_SLslow = 3.4./25; %[ms]

J_diff_cyt = ([Caio(1),Caio(1:end-1)] + [Caio(2:end),Caio(end)] - 2.*Caio)./tau_seg_cyt;
J_diff_SR = ([Ca_sro(1),Ca_sro(1:end-1)] + [Ca_sro(2:end),Ca_sro(end)] - 2.*Ca_sro)./tau_seg_SR; 
J_diff_SL = ([Caslo(1),Caslo(1:end-1)] + [Caslo(2:end),Caslo(end)] - 2.*Caslo)./tau_seg_SLslow;

Caslo = Caslo;%  ...
%     + J_diff_SL;

Caio = Caio ...
    + J_diff_cyt;


Ca_sro = Ca_sro ...
    + J_diff_SR; 
%%
%update system
output2 = 0;
for NoSegments = 1:segments
    obj.output(39,NoSegments) = Caio(NoSegments);
    obj.output(38,NoSegments) = Caslo(NoSegments);
    obj.output(32,NoSegments) = Ca_sro(NoSegments);
    output2 = output2 + obj.Shannon(NoSegments).initialCond_past(1);
end

% obj.output(1,:) = output./segments + I_app;
obj.Vmo = output2./segments;
obj.output(1,:) = obj.Vmo;
obj.CaT = obj.output(39,:);

for NoSegments = 1:segments
    obj.Shannon(NoSegments).initialCond = obj.output(:,NoSegments);
    obj.Shannon(NoSegments).Vmo = obj.Vmo;
end