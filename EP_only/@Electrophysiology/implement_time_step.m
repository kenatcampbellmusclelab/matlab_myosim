function implement_time_step(obj,time_step)

%update time
obj.time = obj.time + time_step;

% Cycle through the segments implementing RU electrophysiology
parfor hs_counter = 1:obj.segmentsCounter
    
    %call EP
    obj.Shannon(hs_counter).implement_time_step(time_step, ...
        obj.segmentsCounter,hs_counter);
end

%call EP
obj.Voigt(time_step);