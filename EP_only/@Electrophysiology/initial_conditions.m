function initial_conditions(obj)

        if isempty(obj.initialCond) == 1

            %% Initial conditions
            mo=1.405627e-3;
            ho= 9.867005e-1;
            jo=9.915620e-1; 
            do=7.175662e-6; 
            fo=1.000681; 
            fcaBjo=2.421991e-2;
            fcaBslo=1.452605e-2;
            xtoso=4.051574e-3;
            ytoso=9.945511e-1; 
            xtofo=4.051574e-3; 
            ytofo= 9.945511e-1; 
            xkro=8.641386e-3; 
            xkso= 5.412034e-3;
            RyRro=8.884332e-1;
            RyRoo=8.156628e-7; 
            RyRio=1.024274e-7; 
            NaBjo=3.539892;
            NaBslo=7.720854e-1; 
            TnClo=8.773191e-3; 
            TnCHco=1.078283e-1; 
            TnCHmo=1.524002e-2; 
            CaMo=2.911916e-4; 
            Myoco=1.298754e-3; 
            Myomo=1.381982e-1;
            SRBo=2.143165e-3; 
            SLLjo=9.566355e-3; 
            SLLslo=1.110363e-1; 
            SLHjo=7.347888e-3; 
            SLHslo=7.297378e-2; 
            Csqnbo= 1.242988;
            Ca_sro=5.545201e-1.*1.75; %0.9 %% changed
            Najo=8.80329; 
            Naslo=8.80733; 
            Naio=8.80853; 
            Kio=135; 
            Cajo=1.737475e-4; 
            Caslo= 1.031812e-4; 
            Caio=8.597401e-5; 
            Vmo=-8.556885e+1; 
            rtoso=0.9946; 
            ICajuncinto=0; 
            ICaslinto=0;

            %%
            % Gating variables   
            y00 = Vmo;
            %   1       2       3       4       5       6       7       8       9       10      11      12      13
            %%   m       h       j       d       f       fcaBj   fcaBsl   xtos    ytos    xtof    ytof    xkr     xks   
            %y10=[1.2e-3;0.99;   0.99;   0.0;    1.0;    0.0141; 0.0141;     0;      1;      0.0;    1.0;    0.0;    6e-3;];
            y10=[mo; ho; jo; do; fo; fcaBjo; fcaBslo; xtoso; ytoso; xtofo; ytofo; xkro; xkso;];  
            y10 = repelem(y10,2); 
            % RyR and Buffering variables
            %   14      15      16      17      18      19      20      21      22      23      24
            %%   RyRr    RyRo    RyRi    NaBj    NaBsl   TnCL    TnCHc   TnCHm   CaM     Myoc    Myom  
            y20=[RyRro; RyRoo; RyRio; NaBjo; NaBslo; TnClo; TnCHco; TnCHmo; CaMo; Myoco; Myomo;];           
            y20 = repelem(y20,2); 
            %y20=[1;     0;      0;      1.8;   0.8;    0.012;   0.112;  0.01;   0.4e-3; 1.9e-3; 0.135;];
            % More buffering variables
            %   25      26      27      28      29      30
            %%   SRB     SLLj   SLLsl    SLHj    SLHsl  Csqnb
            y30=[SRBo; SLLjo; SLLslo; SLHjo; SLHslo; Csqnbo];
            y30 = repelem(y30,2); 
            %y30=[3.3e-3; 0.012; 0.012; 0.13;  0.13;  1.5;];
            %   Intracellular concentrations./ Membrane voltage
            %    31      32      33      34      35      36      37     38       39    40    
            %%    Ca_sr   Naj     Nasl    Nai     Ki      Caj     Casl    Cai     Vm    rtos  
            y40=[Ca_sro; Najo; Naslo; Naio; Kio; Cajo; Caslo; Caslo; Caio; rtoso; ];    
            y40 = repelem(y40,2); 
            %y40=[0.9;    8.8;    8.8;    8.8;    135;    0.1e-3; 0.1e-3; 0.1e-3; -88;  0.89; 0;          0;];


            %% Put everything together
            obj.initialCond= [y00;y10;y20;y30;y40];
            
            
            % Nernst Potentials
            obj.parameters.ena_junc = (1./obj.parameters.FoRT).*log(obj.parameters.Nao./Najo);     % [mV]
            obj.parameters.ena_sl = (1./obj.parameters.FoRT).*log(obj.parameters.Nao./Naslo);       % [mV]
            obj.parameters.ek = (1./obj.parameters.FoRT).*log(obj.parameters.Ko./Kio);	        % [mV]
            obj.parameters.eca_junc = (1./obj.parameters.FoRT./2).*log(obj.parameters.Cao./Cajo);   % [mV]
            obj.parameters.eca_sl = (1./obj.parameters.FoRT./2).*log(obj.parameters.Cao./Caslo);     % [mV]
            obj.parameters.ecl = (1./obj.parameters.FoRT).*log(obj.parameters.Cli./obj.parameters.Clo);            % [mV]
        end
