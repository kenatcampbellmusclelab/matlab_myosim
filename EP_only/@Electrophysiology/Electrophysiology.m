classdef Electrophysiology < handle
    
    properties
        % These are properties that can be accessed from outside the
        % EP class
        
        CaT;
        TnC;
        time = 0;
        
        % A holder for an array of half_sarcomeres
        Shannon = EPShannon();
    end
    
    properties (SetAccess = private)
        % These are properties that can only be accessed from within the
        % EP class
        
        beats = 1;
        output;
        Vmo;
        
        
    end
    
    properties(Constant)
        segmentsCounter = 10;
    end
    
    methods
        
        % Constructor
        function obj = Electrophysiology(varargin)
           for hs_counter = 1:obj.segmentsCounter
                obj.Shannon(hs_counter) = EPShannon();
            end 
        end
        % other methods
        Voigt(obj,timestep);
        
        calcCurrents(obj,t,y);
        
        implement_time_step(obj,time_step);
    end
end