function simulation_driver
% clear all
% close all

% Create the muscle
EP = Electrophysiology()

dt = 1;
% Loop along simulation with a progress bar
t(1) = 0;
simCa = [];
simV = [];
for i=1:2500 %numel(dt)
    t(i+1) = i*dt;%sum(dt(1:i))
    
    % Implement the time-step
    EP.implement_time_step(t(i+1)-t(i));
    
    for counter = 1:10 
        simCa(i,counter) = [EP.Shannon(counter).initialCond(39)];
        simV(i,counter) =[EP.Shannon(counter).initialCond(1)];
    end
    
        figure(1);
        hold on
        subplot(2,1,1);hold on;plot(t(2:end),simCa.*1000)
        subplot(2,1,2);hold on;plot(t(2:end),simV)
    
%     % Draw as we go
%     if ((mod(i,drawing_skip)==0) || (i==length(dml)))
%         if (~exist('sp'))
%             addpath('/Users/viviane/Dropbox/Macy/VoigtCampbell/utilities');
%             sp = initialise_publication_quality_figure( ...
%                 'no_of_panels_wide',2, ...
%                 'no_of_panels_high',4, ...
%                 'right_margin',1, ...
%                 'x_to_y_axes_ratio',2, ...
%                 'axes_padding_top',0.7, ...
%                 'axes_padding_bottom',0.3, ...
%                 'axes_padding_left',1, ...
%                 'axes_padding_right',0.5);
%         end
%         
%         subplot(sp(1));
%         cla;
%         plot(sim.time_s,sim.muscle_length,'g-', ...
%                 'LineWidth',trace_line_width);
%         xlim([0 max(sim.time_s)]);
%         ylabel('Muscle length');
% 
%         subplot(sp(3));
%         cla
%         plot(sim.time_s,sim.muscle_force,'g-', ...
%                 'LineWidth',trace_line_width);
%         xlim([0 max(sim.time_s)]);
%         ylabel('Muscle force');
%         
%         subplot(sp(5));
%         cla
%         hold on;
%         for j=1:m.no_of_half_sarcomeres
%             plot(sim.time_s,sim.hs_force(:,j),'-','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%             plot(sim.time_s,sim.cb_force(:,j),':','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%             plot(sim.time_s,sim.pas_force(:,j),'--','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%         end
%         ylabel('Half-sarcomere forces');
%         xlim([0 max(sim.time_s)]);
% 
%         subplot(sp(7));
%         cla
%         hold on
%         for j=1:m.no_of_half_sarcomeres
%             plot(sim.time_s,sim.hs_length(:,j),'-','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%         end
%         ylabel('Half-sarcomere lengths');
%         xlim([0 max(sim.time_s)]);
%         
%         subplot(sp(2));
%         cla;
%         hold on;
%         for j=1:m.no_of_half_sarcomeres
%             plot(sim.time_s,sim.f_overlap(:,j),':','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%             plot(sim.time_s,sim.f_activated(:,j),'-','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%             plot(sim.time_s,sim.f_bound(:,j),'--','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%         end
%         title({'Dotted - overlap','Solid - activated','Dashed - bound'});
%         xlim([0 max(sim.time_s)]);
%        
%         subplot(sp(4));
%         cla
%         hold on;
%         for j=1:m.no_of_half_sarcomeres
%             plot(sim.time_s,-log10(sim.Ca(:,j)),'-','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%         end
%         ylabel('pCa');
%         xlim([0 max(sim.time_s)]);
%         
%         subplot(sp(6));
%         cla
%         hold on;
%         for j=1:m.no_of_half_sarcomeres
%             plot(sim.time_s,sim.M1(:,j),':','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%             plot(sim.time_s,sim.M2(:,j),'-','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%             plot(sim.time_s,sim.M3(:,j),'--','Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%         end
%         title({'Dotted - M1','Solid - M2','Dashed - M3'});
%         xlim([0 max(sim.time_s)]);
%         
%         subplot(sp(8));
%         cla
%         hold on;
%         for j=1:m.no_of_half_sarcomeres
%             plot(sim.myofilaments.x,squeeze(sim.pops(i,j,:)),'-', ...
%                 'Color',cm(j,:), ...
%                 'LineWidth',trace_line_width);
%         end
%         ylabel('M3 distributions');
%         xlim([min(sim.myofilaments.x) max(sim.myofilaments.x)]);
%         ylim([0 0.05]);
%         
%         drawnow;
   
%         if (~isempty(image_dump_folder))
%             output_file_string = ...
%                 fullfile(image_dump_folder,sprintf('frame_%.0f',i));
%             print(output_file_string,'-dpng','-r150');
%         end
    end
end
