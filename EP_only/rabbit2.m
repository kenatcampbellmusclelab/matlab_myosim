function output = rabbit2()
% global PoUIC3 PoUIC2 PoUIF PoUIM1 PoUIM2 PoUC3 PoUC2 PoUC1 PoUO PoLC3 PoLC2 PoLC1 PoLO
% ParbGal
% ParWT_insDold
% Rabbit E-C coupling model.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%NB: yfinal after 150s 1Hz%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Reference:
% Thomas R. Shannon, Fei Wang, Jose Puglisi, Christopher Weber, Donald M. Bers
% "A Mathematical Treatment of Integrated Ca Dynamics Within the Ventricular Myocyte",
% Biophys J., 87:3351-3371 (2004).
%

p = 0;  % Parameter array for passing nondefault conditions
%% Initial conditions
mo=1.405627e-3;
ho= 9.867005e-1;
jo=9.915620e-1; 
do=7.175662e-6; 
fo=1.000681; 
fcaBjo=2.421991e-2;
fcaBslo=1.452605e-2;
xtoso=4.051574e-3;
ytoso=9.945511e-1; 
xtofo=4.051574e-3; 
ytofo= 9.945511e-1; 
xkro=8.641386e-3; 
xkso= 5.412034e-3;
RyRro=8.884332e-1;
RyRoo=8.156628e-7; 
RyRio=1.024274e-7; 
NaBjo=3.539892;
NaBslo=7.720854e-1; 
TnCLo=8.773191e-3; 
TnCHco=1.078283e-1; 
TnCHmo=1.524002e-2; 
CaMo=2.911916e-4; 
Myoco=1.298754e-3; 
Myomo=1.381982e-1;
SRBo=2.143165e-3; 
SLLjo=9.566355e-3; 
SLLslo=1.110363e-1; 
SLHjo=7.347888e-3; 
SLHslo=7.297378e-2; 
Csqnbo= 1.242988;
Ca_sro=5.545201e-1; 
Najo=8.80329; 
Naslo=8.80733; 
Naio=8.80853; 
Kio=135; 
Cajo=1.737475e-4; 
Caslo= 1.031812e-4; 
Caio=8.597401e-5; 
Vmo=-8.556885e+1; 
rtoso=0.9946; 


% Gating variables      
%   1       2       3       4       5       6       7       8       9       10      11      12      13
%%   m       h       j       d       f       fcaBj   fcaBsl   xtos    ytos    xtof    ytof    xkr     xks   
%y10=[1.2e-3;0.99;   0.99;   0.0;    1.0;    0.0141; 0.0141;     0;      1;      0.0;    1.0;    0.0;    6e-3;];
y10=[Vmo; mo; ho; jo; do; fo; fcaBjo; fcaBslo; xtoso; ytoso; xtofo; ytofo; xkro; xkso;];   
% RyR and Buffering variables
%   14      15      16      17      18      19      20      21      22      23      24
%%   RyRr    RyRo    RyRi    NaBj    NaBsl   TnCL    TnCHc   TnCHm   CaM     Myoc    Myom  
y20=[RyRro; RyRoo; RyRio; NaBjo; NaBslo; TnCLo; TnCHco; TnCHmo; CaMo; Myoco; Myomo;];           
%y20=[1;     0;      0;      1.8;   0.8;    0.012;   0.112;  0.01;   0.4e-3; 1.9e-3; 0.135;];
% More buffering variables
%   25      26      27      28      29      30
%%   SRB     SLLj   SLLsl    SLHj    SLHsl  Csqnb
y30=[SRBo; SLLjo; SLLslo; SLHjo; SLHslo; Csqnbo];
%y30=[3.3e-3; 0.012; 0.012; 0.13;  0.13;  1.5;];
%   Intracellular concentrations/ Membrane voltage
%    31      32      33      34      35      36      37     38       39    40    
%%    Ca_sr   Naj     Nasl    Nai     Ki      Caj     Casl    Cai     Vm    rtos  
y40=[Ca_sro; Najo; Naslo; Naio; Kio; Cajo; Caslo; Caio; rtoso; ];    
%y40=[0.9;    8.8;    8.8;    8.8;    135;    0.1e-3; 0.1e-3; 0.1e-3; -88;  0.89; 0;          0;];
  
% Put everything together
y0  = [y10;y20;y30;y40];    
% load('yfinal'); % load output of previous simulation saved as yfinal.mat
% y0 = yfinal;

%% Single Run Simulation
tspan = [0;250];
options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','on'); 
[t,y] = ode15s(@f,tspan,y0,options,p);

yfinal = y(end,:);
output = yfinal;
% save 'yfinal'
%voltage=y(:,39);
%save 'voltage'
%save 't'
currents = calcCurrents(t,y,p);
figure(1);
hold on
subplot(2,1,2); hold on;plot(t,y(:,1)); title('Voltage');
subplot(2,1,1); hold on;plot(t,y(:,39).*1000); title('Cyto calcium');


% subplot(4,1,1); plot(t,y(:,1)); title('Voltage');
% subplot(4,1,2); plot(t,y(:,39)); title('Cyto calcium');
% subplot(4,1,3); plot(t,y(:,33),t,y(:,34),t,y(:,35)); title('Na concs');
% subplot(4,1,4); plot(t,y(:,37),t,y(:,38),t,y(:,32)); title('Ca concs');

function output = f(t,y,p,runType)

ydot = zeros(size(y));

ydot = zeros(size(y));

%% input values
Vmo             = y(1)

mo                 = y(2);
ho                 = y(3);
jo                 = y(4);
do                 = y(5);
fo                 = y(6);
fcaBjo             = y(7);
fcaBslo            = y(8);
xtoso              = y(9);
ytoso              = y(10);
xtofo              = y(11);
ytofo              = y(12);
xkro               = y(13);
xkso               = y(14);
RyRro              = y(15);
RyRoo              = y(16);
RyRio              = y(17);
NaBjo              = y(18);
NaBslo             = y(19);
TnClo              = y(20);
TnCHco             = y(21);
TnCHmo             = y(22);
CaMo               = y(23);
Myoco              = y(24);
Myomo              = y(25);
SRBo               = y(26);
SLLjo              = y(27);
SLLslo             = y(28);
SLHjo              = y(29);
SLHslo             = y(30);
Csqnbo             = y(31);
Ca_sro             = y(32);
Najo               = y(33);
Naslo              = y(34);
Naio               = y(35);
Kio                = y(36);
Cajo               = y(37);
Caslo              = y(38);
Caio               = y(39);
rtoso              = y(40);


%% Model Parameters
% Constants
R = 8314;       % [J/kmol*K]  
Frdy = 96485;   % [C/mol]  
Temp = 310;     % [K]
FoRT = Frdy/R/Temp;
Cmem = 1.3810e-10;   % [F] membrane capacitance
Qpow = (Temp-310)/10;

% Cell geometry
cellLength = 100;     % cell length [um]
cellRadius = 10.25;   % cell radius [um]
junctionLength = 160e-3;  % junc length [um]
junctionRadius = 15e-3;   % junc radius [um]
Vcell = pi*cellRadius^2*cellLength*1e-15;    % [L]
Vmyo = 0.65*Vcell; Vsr = 0.035*Vcell; Vsl = 0.02*Vcell; Vjunc = 0.0539*.01*Vcell; 

J_ca_juncsl = 1/1.2134e12; % [L/msec] = 8.2413e-13
J_ca_slmyo = 1/2.68510e11; % [L/msec] = 3.2743e-12
J_na_juncsl = 1/(1.6382e12/3*100); % [L/msec] = 6.1043e-13
J_na_slmyo = 1/(1.8308e10/3*100);  % [L/msec] = 5.4621e-11

% Fractional currents in compartments
Fjunc = 0.11;   Fsl = 1-Fjunc;
Fjunc_CaL = 0.9; Fsl_CaL = 1-Fjunc_CaL;

% Fixed ion concentrations     
Cli = 15;   % Intracellular Cl  [mM]
Clo = 150;  % Extracellular Cl  [mM]
Ko = 5.4;   % Extracellular K   [mM]
Nao = 140;  % Extracellular Na  [mM]
Cao = 1.8;  % Extracellular Ca  [mM]
Mgi = 1;    % Intracellular Mg  [mM]

% Nernst Potentials
ena_junc = (1/FoRT)*log(Nao/Najo);     % [mV]
ena_sl = (1/FoRT)*log(Nao/Naslo);       % [mV]
ek = (1/FoRT)*log(Ko/Kio);	        % [mV]
eca_junc = (1/FoRT/2)*log(Cao/Cajo);   % [mV]
eca_sl = (1/FoRT/2)*log(Cao/Caslo);     % [mV]
ecl = (1/FoRT)*log(Cli/Clo);            % [mV]

% Na transport parameters

GNa=16;
GNaB = 0.297e-3;    % [mS/uF] 
IbarNaK = 1.90719;     % [uA/uF]
KmNaip = 11;         % [mM]
KmKo = 1.5;         % [mM]
Q10NaK = 1.63;  
Q10KmNai = 1.39;

%% K current parameters
pNaK = 0.01833;      
GtoSlow = 0.06*1;     % [mS/uF] %0.09 CaMKII
GtoFast = 0.02*1;     % [mS/uF] 
gkp = 0.001;

% Cl current parameters
GClCa = 0.109625;   % [mS/uF]
GClB = 9e-3;        % [mS/uF]
KdClCa = 100e-3;    % [mM]

% I_Ca parameters
pNa = 1.5e-8;       % [cm/sec]
pCa = 5.4e-4;       % [cm/sec]
pK = 2.7e-7;        % [cm/sec]
Q10CaL = 1.8;       

% Ca transport parameters
IbarNCX = 9.0;      % [uA/uF]
KmCai = 3.59e-3;    % [mM]
KmCao = 1.3;        % [mM]
KmNai = 12.29;      % [mM]
KmNao = 87.5;       % [mM]
ksat = 0.27;        % [none]  
nu = 0.35;          % [none]
Kdact = 0.256e-3;   % [mM] 
Q10NCX = 1.57;      % [none]
IbarSLCaP = 0.0673; % [uA/uF](2.2 umol/L cytosol/sec) 
KmPCa = 0.5e-3;     % [mM] 
GCaB = 2.513e-4;    % [uA/uF] 
Q10SLCaP = 2.35;    % [none]

% SR flux parameters
Q10SRCaP = 2.6;          % [none]
Vmax_SRCaP = 5.3114e-3;  % [mM/msec] (286 umol/L cytosol/sec)
Kmf = 0.246e-3;          % [mM] default
%Kmf = 0.175e-3;          % [mM]
Kmr = 1.7;               % [mM]L cytosol
hillSRCaP = 1.787;       % [mM]
ks = 25;                 % [1/ms]      
koCa = 10;               % [mM^-2 1/ms]   %default 10   modified 20
kom = 0.06;              % [1/ms]     
kiCa = 0.5;              % [1/mM/ms]
kim = 0.005;             % [1/ms]
ec50SR = 0.45;           % [mM]

% Buffering parameters
% Note: we are using [1/ms] and [1/mM/ms], which differs from that in the paper 
% koff: [1/s] = 1e-3*[1/ms];  kon: [1/uM/s] = [1/mM/ms]
Bmax_Naj = 7.561;       % [mM]  % Na buffering
Bmax_Nasl = 1.65;       % [mM]
koff_na = 1e-3;         % [1/ms]
kon_na = 0.1e-3;        % [1/mM/ms]
Bmax_TnClow = 70e-3;    % [mM]                      % TnC low affinity
koff_tncl = 19.6e-3;    % [1/ms] 
kon_tncl = 32.7;        % [1/mM/ms]
Bmax_TnChigh = 140e-3;  % [mM]                      % TnC high affinity 
koff_tnchca = 0.032e-3; % [1/ms] 
kon_tnchca = 2.37;      % [1/mM/ms]
koff_tnchmg = 3.33e-3;  % [1/ms] 
kon_tnchmg = 3e-3;      % [1/mM/ms]
Bmax_CaM = 24e-3;       % [mM]  % CaM buffering
koff_cam = 238e-3;      % [1/ms] 
kon_cam = 34;           % [1/mM/ms]
Bmax_myosin = 140e-3;   % [mM]                      % Myosin buffering
koff_myoca = 0.46e-3;   % [1/ms]
kon_myoca = 13.8;       % [1/mM/ms]
koff_myomg = 0.057e-3;  % [1/ms]
kon_myomg = 0.0157;     % [1/mM/ms]
Bmax_SR = 19*.9e-3;     % [mM] (Bers text says 47e-3) 19e-3
koff_sr = 60e-3;        % [1/ms]
kon_sr = 100;           % [1/mM/ms]
Bmax_SLlowsl = 37.4e-3*Vmyo/Vsl;        % [mM]    % SL buffering
Bmax_SLlowj = 4.6e-3*Vmyo/Vjunc*0.1;    % [mM]    
koff_sll = 1300e-3;     % [1/ms]
kon_sll = 100;          % [1/mM/ms]
Bmax_SLhighsl = 13.4e-3*Vmyo/Vsl;       % [mM] 
Bmax_SLhighj = 1.65e-3*Vmyo/Vjunc*0.1;  % [mM] 
koff_slh = 30e-3;       % [1/ms]
kon_slh = 100;          % [1/mM/ms]
Bmax_Csqn = 140e-3*Vmyo/Vsr;            % [mM] % Bmax_Csqn = 2.6;      % Csqn buffering
koff_csqn = 65;         % [1/ms] 
kon_csqn = 100;         % [1/mM/ms] 


%% Membrane Currents
% I_Na: Fast Na Current
am = 0.32*(Vmo+47.13)/(1-exp(-0.1*(Vmo+47.13)));
bm = 0.08*exp(-Vmo/11);
if Vmo >= -40
    ah = 0; aj = 0;
    bh = 1/(0.13*(1+exp(-(Vmo+10.66)/11.1)));
    bj = 0.3*exp(-2.535e-7*Vmo)/(1+exp(-0.1*(Vmo+32)));
else
    ah = 0.135*exp((80+Vmo)/-6.8);
    bh = 3.56*exp(0.079*Vmo)+3.1e5*exp(0.35*Vmo);
    aj = (-1.2714e5*exp(0.2444*Vmo)-3.474e-5*exp(-0.04391*Vmo))*(Vmo+37.78)/(1+exp(0.311*(Vmo+79.23)));
    bj = 0.1212*exp(-0.01052*Vmo)/(1+exp(-0.1378*(Vmo+40.14)));
end
dmo = am*(1-mo)-bm*mo;
dho = ah*(1-ho)-bh*ho;
djo = aj*(1-jo)-bj*jo;


I_Na_junc = Fjunc*GNa*mo^3*ho*jo*(Vmo-ena_junc);
I_Na_sl = Fsl*GNa*mo^3*ho*jo*(Vmo-ena_sl);
I_Na = I_Na_junc+I_Na_sl;



% I_nabk: Na Background Current
I_nabk_junc = Fjunc*GNaB*(Vmo-ena_junc);
I_nabk_sl = Fsl*GNaB*(Vmo-ena_sl);
I_nabk = I_nabk_junc+I_nabk_sl;

% I_nak: Na/K Pump Current
sigma = (exp(Nao/67.3)-1)/7;
fnak = 1/(1+0.1245*exp(-0.1*Vmo*FoRT)+0.0365*sigma*exp(-Vmo*FoRT));
I_nak_junc = Fjunc*IbarNaK*fnak*Ko /(1+(KmNaip/Najo)^4) /(Ko+KmKo);
I_nak_sl = Fsl*IbarNaK*fnak*Ko /(1+(KmNaip/Naslo)^4) /(Ko+KmKo);
I_nak = I_nak_junc+I_nak_sl;

% I_kr: Rapidly Activating K Current
gkr = 0.03*sqrt(Ko/5.4);
xrss = 1/(1+exp(-(Vmo+50)/7.5));
tauxr = 1/(1.38e-3*(Vmo+7)/(1-exp(-0.123*(Vmo+7)))+6.1e-4*(Vmo+10)/(exp(0.145*(Vmo+10))-1));
dxkro = (xrss-xkro)/tauxr;
rkr = 1/(1+exp((Vmo+33)/22.4));
I_kr = gkr*xkro*rkr*(Vmo-ek);

% I_ks: Slowly Activating K Current
pcaks_junc = -log10(Cajo)+3.0; 
pcaks_sl = -log10(Caslo)+3.0;  
gks_junc = 0.07*(0.057 +0.19/(1+ exp((-7.2+pcaks_junc)/0.6)));
gks_sl = 0.07*(0.057 +0.19/(1+ exp((-7.2+pcaks_sl)/0.6))); 
eks = (1/FoRT)*log((Ko+pNaK*Nao)/(Kio+pNaK*Naio));	
xsss = 1/(1+exp(-(Vmo-1.5)/16.7));
tauxs = 1/(7.19e-5*(Vmo+30)/(1-exp(-0.148*(Vmo+30)))+1.31e-4*(Vmo+30)/(exp(0.0687*(Vmo+30))-1)); 
dxkso = (xsss-xkso)/tauxs;
I_ks_junc = Fjunc*gks_junc*xkso^2*(Vmo-eks);
I_ks_sl = Fsl*gks_sl*xkso^2*(Vmo-eks);
I_ks = I_ks_junc+I_ks_sl;

%I_kp: Plateau K current
kp_kp = 1/(1+exp(7.488-Vmo/5.98));
I_kp_junc = Fjunc*gkp*kp_kp*(Vmo-ek);
I_kp_sl = Fsl*gkp*kp_kp*(Vmo-ek);
I_kp = I_kp_junc+I_kp_sl;

%% I_to: Transient Outward K Current (slow and fast components)
xtoss = 1/(1+exp(-(Vmo+3.0)/15));
ytoss = 1/(1+exp((Vmo+33.5)/10));
rtoss = 1/(1+exp((Vmo+33.5)/10));
tauxtos = 9/(1+exp((Vmo+3.0)/15))+0.5;
tauytos = 3e3/(1+exp((Vmo+60.0)/10))+30;
%tauytos = 182/(1+exp((Vmo+33.5)/10))+1;
taurtos = 2.8e3/(1+exp((Vmo+60.0)/10))+220; %Fei changed here!! time-dependent gating variable
%taurtos =8085/(1+exp((Vmo+33.5)/10))+313;
dxtoso = (xtoss-xtoso)/tauxtos;
dytoso = (ytoss-ytoso)/tauytos;
drtoso= (rtoss-rtoso)/taurtos; %Fei changed here!! time-dependent gating variable
I_tos = GtoSlow*xtoso*(ytoso+0.5*rtoso)*(Vmo-ek);    % [uA/uF]

tauxtof = 3.5*exp(-Vmo*Vmo/30/30)+1.5;
%tauxtof = 3.5*exp(-((Vmo+3)/30)^2)+1.5;
tauytof = 20.0/(1+exp((Vmo+33.5)/10))+20.0;
%tauytof = 20.0/(1+exp((Vmo+33.5)/10))+20.0;
dxtofo = (xtoss-xtofo)/tauxtof;
dytofo = (ytoss-ytofo)/tauytof;
I_tof = GtoFast*xtofo*ytofo*(Vmo-ek);
I_to = I_tos + I_tof;

% I_ki: Time-Independent K Current
aki = 1.02/(1+exp(0.2385*(Vmo-ek-59.215)));
bki =(0.49124*exp(0.08032*(Vmo+5.476-ek)) + exp(0.06175*(Vmo-ek-594.31))) /(1 + exp(-0.5143*(Vmo-ek+4.753)));
kiss = aki/(aki+bki);
I_ki = 0.9*sqrt(Ko/5.4)*kiss*(Vmo-ek);

% I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current
I_ClCa_junc = Fjunc*GClCa/(1+KdClCa/Cajo)*(Vmo-ecl);
I_ClCa_sl = Fsl*GClCa/(1+KdClCa/Caslo)*(Vmo-ecl);
I_ClCa = I_ClCa_junc+I_ClCa_sl;
I_Clbk = GClB*(Vmo-ecl);

%% I_Ca: L-type Calcium Current
dss = 1/(1+exp(-(Vmo+14.5)/6.0));
taud = dss*(1-exp(-(Vmo+14.5)/6.0))/(0.035*(Vmo+14.5));
fss = 1/(1+exp((Vmo+35.06)/3.6))+0.6/(1+exp((50-Vmo)/20));
tauf = 1/(0.0197*exp( -(0.0337*(Vmo+14.5))^2 )+0.02);
ddo = (dss-do)/taud;
dfo = (fss-fo)/tauf;
dfcaBjo = 1.7*Cajo*(1-fcaBjo)-11.9e-3*fcaBjo; % fCa_junc   koff!!!!!!!!
dfcaBslo = 1.7*Caslo*(1-fcaBslo)-11.9e-3*fcaBslo; % fCa_sl
fcaCaMSL= 0.1/(1+(0.01/Caslo));
fcaCaj= 0.1/(1+(0.01/Cajo));
fcaCaMSL=0;
fcaCaj= 0;
%fcaBjo=0;
%fcaBslo=0;
ibarca_j = pCa*4*(Vmo*Frdy*FoRT) * (0.341*Cajo*exp(2*Vmo*FoRT)-0.341*Cao) /(exp(2*Vmo*FoRT)-1);
ibarca_sl = pCa*4*(Vmo*Frdy*FoRT) * (0.341*Caslo*exp(2*Vmo*FoRT)-0.341*Cao) /(exp(2*Vmo*FoRT)-1);
ibark = pK*(Vmo*Frdy*FoRT)*(0.75*Kio*exp(Vmo*FoRT)-0.75*Ko) /(exp(Vmo*FoRT)-1);
ibarna_j = pNa*(Vmo*Frdy*FoRT) *(0.75*Najo*exp(Vmo*FoRT)-0.75*Nao)  /(exp(Vmo*FoRT)-1);
ibarna_sl = pNa*(Vmo*Frdy*FoRT) *(0.75*Naslo*exp(Vmo*FoRT)-0.75*Nao)  /(exp(Vmo*FoRT)-1);
I_Ca_junc = (Fjunc_CaL*ibarca_j*do*fo*((1-fcaBjo)+fcaCaj)*Q10CaL^Qpow)*0.45*1;
I_Ca_sl = (Fsl_CaL*ibarca_sl*do*fo*((1-fcaBslo)+fcaCaMSL)*Q10CaL^Qpow)*0.45*1;
I_Ca = I_Ca_junc+I_Ca_sl;
I_CaK = (ibark*do*fo*(Fjunc_CaL*(fcaCaj+(1-fcaBjo))+Fsl_CaL*(fcaCaMSL+(1-fcaBslo)))*Q10CaL^Qpow)*0.45*1;
I_CaNa_junc = (Fjunc_CaL*ibarna_j*do*fo*((1-fcaBjo)+fcaCaj)*Q10CaL^Qpow)*0.45*1;
I_CaNa_sl = (Fsl_CaL*ibarna_sl*do*fo*((1-fcaBslo)+fcaCaMSL)*Q10CaL^Qpow)*.45*1;
I_CaNa = I_CaNa_junc+I_CaNa_sl;
I_Catot = I_Ca+I_CaK+I_CaNa;

% I_ncx: Na/Ca Exchanger flux
Ka_junc = 1/(1+(Kdact/Cajo)^3);
Ka_sl = 1/(1+(Kdact/Caslo)^3);
s1_junc = exp(nu*Vmo*FoRT)*Najo^3*Cao;
s1_sl = exp(nu*Vmo*FoRT)*Naslo^3*Cao;
s2_junc = exp((nu-1)*Vmo*FoRT)*Nao^3*Cajo;
s3_junc = KmCai*Nao^3*(1+(Najo/KmNai)^3) + KmNao^3*Cajo*(1+Cajo/KmCai)+KmCao*Najo^3+Najo^3*Cao+Nao^3*Cajo;
s2_sl = exp((nu-1)*Vmo*FoRT)*Nao^3*Caslo;
s3_sl = KmCai*Nao^3*(1+(Naslo/KmNai)^3) + KmNao^3*Caslo*(1+Caslo/KmCai)+KmCao*Naslo^3+Naslo^3*Cao+Nao^3*Caslo;
I_ncx_junc = Fjunc*IbarNCX*Q10NCX^Qpow*Ka_junc*(s1_junc-s2_junc)/s3_junc/(1+ksat*exp((nu-1)*Vmo*FoRT));
I_ncx_sl = Fsl*IbarNCX*Q10NCX^Qpow*Ka_sl*(s1_sl-s2_sl)/s3_sl/(1+ksat*exp((nu-1)*Vmo*FoRT));
I_ncx = I_ncx_junc+I_ncx_sl;

% I_pca: Sarcolemmal Ca Pump Current
I_pca_junc = Fjunc*Q10SLCaP^Qpow*IbarSLCaP*Cajo^1.6/(KmPCa^1.6+Cajo^1.6);
I_pca_sl = Fsl*Q10SLCaP^Qpow*IbarSLCaP*Caslo^1.6/(KmPCa^1.6+Caslo^1.6);
I_pca = I_pca_junc+I_pca_sl;

% I_cabk: Ca Background Current
I_cabk_junc = Fjunc*GCaB*(Vmo-eca_junc);
I_cabk_sl = Fsl*GCaB*(Vmo-eca_sl);
I_cabk = I_cabk_junc+I_cabk_sl;

%% SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
MaxSR = 15; MinSR = 1;
kCaSR = MaxSR - (MaxSR-MinSR)/(1+(ec50SR/Ca_sro)^2.5);
koSRCa = koCa/kCaSR;
kiSRCa = kiCa*kCaSR;
RI = 1-RyRro-RyRoo-RyRio;
dRyRro = (kim*RI-kiSRCa*Cajo*RyRro)-(koSRCa*Cajo^2*RyRro-kom*RyRoo);   % R
dRyRoo = (koSRCa*Cajo^2*RyRro-kom*RyRoo)-(kiSRCa*Cajo*RyRoo-kim*RyRio);% O
dRyRio = (kiSRCa*Cajo*RyRoo-kim*RyRio)-(kom*RyRio-koSRCa*Cajo^2*RI);   % I
J_SRCarel = ks*RyRoo*(Ca_sro-Cajo);          % [mM/ms]

J_serca = Q10SRCaP^Qpow*Vmax_SRCaP*((Caio/Kmf)^hillSRCaP-(Ca_sro/Kmr)^hillSRCaP)...
    /(1+(Caio/Kmf)^hillSRCaP+(Ca_sro/Kmr)^hillSRCaP);
J_SRleak = 5.348e-6*(Ca_sro-Cajo);           %   [mM/ms]

%% Sodium and Calcium Buffering
dNaBjo = kon_na*Najo*(Bmax_Naj-NaBjo)-koff_na*NaBjo;        % NaBj      [mM/ms]
dNaBslo = kon_na*Naslo*(Bmax_Nasl-NaBslo)-koff_na*NaBslo;       % NaBsl     [mM/ms]

% Cytosolic Ca Buffers
dTnClo = kon_tncl*Caio*(Bmax_TnClow-TnClo)-koff_tncl*TnClo;            % TnCL      [mM/ms]
dTnCHco = kon_tnchca*Caio*(Bmax_TnChigh-TnCHco-TnCHmo)-koff_tnchca*TnCHco; % TnCHc     [mM/ms]
dTnCHmo = kon_tnchmg*Mgi*(Bmax_TnChigh-TnCHco-TnCHmo)-koff_tnchmg*TnCHmo;   % TnCHm     [mM/ms]
dCaMo = kon_cam*Caio*(Bmax_CaM-CaMo)-koff_cam*CaMo;                 % CaM       [mM/ms]
dMyoco = kon_myoca*Caio*(Bmax_myosin-Myoco-Myomo)-koff_myoca*Myoco;    % Myosin_ca [mM/ms]
dMyomo = kon_myomg*Mgi*(Bmax_myosin-Myoco-Myomo)-koff_myomg*Myomo;      % Myosin_mg [mM/ms]
dSRBo = kon_sr*Caio*(Bmax_SR-SRBo)-koff_sr*SRBo;                    % SRB       [mM/ms]
J_CaB_cytosol = dTnClo+dTnCHco+dTnCHmo+dCaMo+dMyoco+dMyomo+dSRBo;

% Junctional and SL Ca Buffers
dSLLjo = kon_sll*Cajo*(Bmax_SLlowj-SLLjo)-koff_sll*SLLjo;       % SLLj      [mM/ms]
dSLLslo = kon_sll*Caslo*(Bmax_SLlowsl-SLLslo)-koff_sll*SLLslo;      % SLLsl     [mM/ms]
dSLHjo = kon_slh*Cajo*(Bmax_SLhighj-SLHjo)-koff_slh*SLHjo;      % SLHj      [mM/ms]
dSLHslo = kon_slh*Caslo*(Bmax_SLhighsl-SLHslo)-koff_slh*SLHslo;     % SLHsl     [mM/ms]
J_CaB_junction = dSLLjo+dSLHjo;
J_CaB_sl = dSLLslo+dSLHslo;

%% Ion concentrations
% SR Ca Concentrations
dCsqnbo = kon_csqn*Ca_sro*(Bmax_Csqn-Csqnbo)-koff_csqn*Csqnbo;       % Csqn      [mM/ms]
dCa_sro = J_serca-(J_SRleak*Vmyo/Vsr+J_SRCarel)-dCsqnbo;         % Ca_sr     [mM/ms] %Ratio 3 leak current

% Sodium Concentrations
I_Na_tot_junc = I_Na_junc+I_nabk_junc+3*I_ncx_junc+3*I_nak_junc+I_CaNa_junc;   % [uA/uF]
I_Na_tot_sl = I_Na_sl+I_nabk_sl+3*I_ncx_sl+3*I_nak_sl+I_CaNa_sl;   % [uA/uF]

dNajo = -I_Na_tot_junc*Cmem/(Vjunc*Frdy)+J_na_juncsl/Vjunc*(Naslo-Najo)-dNaBjo;
dNaslo = -I_Na_tot_sl*Cmem/(Vsl*Frdy)+J_na_juncsl/Vsl*(Najo-Naslo)...
   +J_na_slmyo/Vsl*(Naio-Naslo)-dNaBslo;
%dNajo = 0;
%dNaslo = 0;
dNaio = J_na_slmyo/Vmyo*(Naslo-Naio);             % [mM/msec] 
%dNaio=0;

% Potassium Concentration
I_K_tot = I_to+I_kr+I_ks+I_ki-2*I_nak+I_CaK+I_kp;     % [uA/uF]
dKio = 0; %-I_K_tot*Cmem/(Vmyo*Frdy);           % [mM/msec]

% Calcium Concentrations
I_Ca_tot_junc = I_Ca_junc+I_cabk_junc+I_pca_junc-2*I_ncx_junc;                   % [uA/uF]
I_Ca_tot_sl = I_Ca_sl+I_cabk_sl+I_pca_sl-2*I_ncx_sl;            % [uA/uF]


%% For the first, leftmost 1 (d=1) connected to membrane 1
J_diff_CytSL = J_ca_slmyo*(Caio-Caslo);
J_diff_juncSL = J_ca_juncsl.*(Cajo-Caslo);

dCajo = -I_Ca_tot_junc.*Cmem./(Vjunc.*2.*Frdy)...
    - J_CaB_junction+(J_SRCarel).*Vsr./Vjunc...
    + J_SRleak.*Vmyo./Vjunc...
    - J_diff_juncSL./Vjunc;

dCaslo = -I_Ca_tot_sl.*Cmem./(Vsl.*2.*Frdy)...
    - J_CaB_sl + J_diff_juncSL./Vsl + J_diff_CytSL./Vsl;

dCaio = -J_serca.*Vsr./Vmyo...
    - J_CaB_cytosol - J_diff_CytSL./Vmyo;

dCa_sro = J_serca...
    - (J_SRleak.*Vmyo./Vsr+J_SRCarel)...
    - dCsqnbo; 


% %% SAC
% %%
% % I_NS (cation-nonselective SAC) current
% E_ns = -10; %mV
% G_K0 = 1./10; %zwischen 0 und 1./10
% G_ns = 0.08;%0.08;
% I_ns = G_ns .* (Vmo - E_ns);
% 
% %SAC_K (K+ selective SAC) current
% I_K0 = G_K0 ./ (1 + exp((19.05 - Vmo)./29.98));
% 
% %SAC current
% I_SAC = (I_ns + I_K0) .* (strainfunc);
% 
% 
% I_SAC = [I_SAC,I_SAC];
% I_SAC = zeros(size([strainfunc,strainfunc]));
% % if (nargin == 6) & strcmp(runType,'currents')
% %     I_SAC = [I_SAC(1:segments);I_SAC(segments+1:end)]';
% % end

I_SAC = 0;

%% Simulation type
protocol = 'pace1';

switch lower(protocol)
    case {'none',''},
        I_app = 0;
    case 'pace1',        % pace w/ current injection at rate 'rate'
		rate = 1e-3;
		if mod(t+990,1/rate) <= 5
            I_app = 9.5;
        else
            I_app = 0.0;
        end
    case 'pace2',        % pace w/ current injection at rate 'rate'
		factor = 2;
        rate = factor*1e-3;
		if (mod(t+900,1/rate) <= 5) & ((t > 5000) & (t < 10000))
            I_app = 12.0;
        elseif (mod(t+900,1/rate*factor*2) <= 5)  & ((t <= 5000) | (t >= 10000))
            I_app = 12.0;
        else
            I_app = 0;
        end        
    case 'pace3',
        rate = 40e-3;
        if (t > 1000) & (t < 2000) & (mod(t+900,1/rate) <= 5) 
                    I_app = 10;    
        elseif (t > 2000) & (mod(t+900,1/rate*10) <= 5)
                    I_app = 10;
                    else
            I_app = 0;
        end
    case 'vclamp',      
		V_hold = -90;
        V_test = 0;
		if (t > 100 & t < 300)
		    V_clamp = V_test;
		else
		    V_clamp = V_hold;
		end
		R_clamp = 0.02;
		I_app = (V_clamp-Vmo)/R_clamp;
    case 'fig5',
        rate = 0.5e-3;
        %if t<=60000
         %   V_clamp=-90;
        %else
        if mod(t,1/rate)<=200
            V_clamp = -20;
		else
		    V_clamp = -90;
        end
        %end
  
    R_clamp = 0.02;
		I_app = (V_clamp-Vmo)/R_clamp;
            
end  

%% Membrane Potential
I_Na_tot = I_Na_tot_junc + I_Na_tot_sl;          % [uA/uF]
I_Cl_tot = I_ClCa+I_Clbk;                        % [uA/uF]
I_Ca_tot = I_Ca_tot_junc+I_Ca_tot_sl;
I_tot = I_Na_tot+I_Cl_tot+I_Ca_tot+I_K_tot+I_SAC;
%dVmo = -(I_Ca_tot+I_K_tot+I_Na_tot-I_app);
dVmo = -(I_tot-I_app);
vmax = dVmo;


%% output vector
ydot(1)  =  dVmo;

%membrane 1
ydot(2)  =  dmo;
ydot(3)  =  dho;
ydot(4)  =  djo;
ydot(5)  =  ddo;
ydot(6)  =  dfo;
ydot(7)  =  dfcaBjo;
ydot(8)  =  dfcaBslo;
ydot(9)  =  dxtoso;
ydot(10)  =  dytoso;
ydot(11)  =  dxtofo;
ydot(12)  =  dytofo;
ydot(13)  =  dxkro;
ydot(14)  =  dxkso;
ydot(15)  =  dRyRro;
ydot(16)  =  dRyRoo;
ydot(17)  =  dRyRio;
ydot(18)  =  dNaBjo;
ydot(19)  =  dNaBslo;
ydot(20)  =  dTnClo;
ydot(21)  =  dTnCHco;
ydot(22)  =  dTnCHmo;
ydot(23)  =  dCaMo;
ydot(24)  =  dMyoco;
ydot(25)  =  dMyomo;
ydot(26)  =  dSRBo;
ydot(27)  =  dSLLjo;
ydot(28)  =  dSLLslo;
ydot(29)  =  dSLHjo;
ydot(30)  =  dSLHslo;
ydot(31)  =  dCsqnbo;
ydot(32)  =  dCa_sro;
ydot(33)  =  dNajo;
ydot(34)  =  dNaslo;
ydot(35)  =  dNaio;
ydot(36)  =  dKio;
ydot(37)  =  dCajo;
ydot(38)  =  dCaslo;
ydot(39)  =  dCaio;
ydot(40)  =  drtoso;


% ----- END EC COUPLING MODEL ---------------
% adjust output depending on the function call

if (nargin == 3)
    output = ydot;
elseif (nargin == 4) & strcmp(runType,'ydot')
    output = ydot;
elseif (nargin == 4) & strcmp(runType,'rates')
    output = r;
elseif (nargin == 4) & strcmp(runType,'currents')
    %currents = [I_Na I_nabk I_nak I_kr I_ks I_kp I_tos I_tof I_ki I_ClCa I_Clbk I_Catot I_ncx I_pca I_cabk J_serca*Vmyo/Vsr];
    %currents = [I_Na I_tof I_tos I_kr I_ks I_ClCa I_Catot J_SRCarel*Vsr/Vmyo J_SRleak RI I_ncx]; 
    currents= [];
    output = currents;
end

%% Calculate timecourse for currents and other intermediates
function currents = calcCurrents(t,y,p)
% After running a simulation, feed the time vector and state variables into
% this function to compute ionic currents, etc.
% currents: [I_Na,I_Catot];
currents=[];
for i=1:size(t)
    if ceil(i/1000)==i/1000
        disp(['t = ',num2str(ceil(t(i)))]);
    end
    currents=[currents;f(t(i),y(i,:),p,'currents')];
end

% end calcCurrents