function PlotFile

load 'strongly_200.mat'

figure(1)
subplot(4,1,1); hold on; plot(sim.time_s,sim.EP(1,:)); title('voltage')
subplot(4,1,2); hold on; plot(sim.time_s,sim.EP(39,:).*1000); title('calcium')
subplot(4,1,3); hold on; plot(sim.time_s,sim.currents(1,:)); title('I_{ncx}')
subplot(4,1,4); hold on; plot(sim.time_s,sim.currents(5,:)); title('L-type')
clear all

load 'strongly_100_koff.mat'

figure(1)
subplot(4,1,1); hold on; plot(sim.time_s,sim.EP(1,:)); title('voltage')
subplot(4,1,2); hold on; plot(sim.time_s,sim.EP(39,:).*1000); title('calcium')
subplot(4,1,3); hold on; plot(sim.time_s,sim.currents(1,:)); title('I_{ncx}')
subplot(4,1,4); hold on; plot(sim.time_s,sim.currents(5,:)); title('L-type')

clear all
load 'rabbit_CL300.mat'

figure(1)
hold on
subplot(4,1,1); hold on;plot(t./1000,y(:,1)); title('voltage')
subplot(4,1,2); hold on;plot(t./1000,y(:,39).*1000); title('calcium')
subplot(4,1,3); hold on;plot(t./1000,currents(:,1)); title('I_{ncx}')
subplot(4,1,4); hold on;plot(t./1000,currents(:,5)); title('L-type')