function simulation_driver

% Set muscle properties
muscle_props.no_of_half_sarcomeres = 3;
muscle_props.series_k_linear = 1e4;         % stiffness of series spring
% in units of N m^-1

% Folder to dump images (keep empty if no images)
image_dump_folder='image_dump';
% image_dump_folder = '';

% Set up display properties
% color map for half-sarcomeres
if (muscle_props.no_of_half_sarcomeres==2)
    cm = [1 0 0 ; 0 0 1];
else
    cm = jet(muscle_props.no_of_half_sarcomeres);
end
drawing_skip = 5;           % Will draw every drawing_skip time_step
trace_line_width = 2;       % trace line thickness

% % Read in Ca transient
% pCa_trace = dlmread('pCa.txt');
% Ca_trace = 10.^-pCa_trace;
% Ca_array = repmat(Ca_trace,[1 muscle_props.no_of_half_sarcomeres]);
% Ca_array(500:700,2)=10^-8;

% Set simulation time-step
CL = 0.25; %cycle length
dt = 0.002*ones(numel([0:0.002:CL]),1);

% Set length change
dml = zeros(numel([0:0.002:CL]),1);
% dml(1000:1100) = 1;
% dml(1101:1200) = -1;

% Create the muscle
m = muscle(muscle_props)
% Initialise the system so that force is balanced in the half-sarcomeres
% and in the series extension
impose_force_balance(m);

% Prepare a structure called sim to hold the results
sim = [];
% Prefill it with the hs.parameters and hs.myofilaments
% in case they are useful
% Set up for simulation
sim.myofilaments = m.hs(1).myofilaments;
sim.parameters = m.hs(1).parameters;
% Prefill the holder arrays with NaNs for drawing
sim.time_s = cumsum(dt);
NaN_array = NaN*ones(numel(dt),1);
sim.muscle_force = NaN_array;
sim.muscle_length = NaN_array;
NaN_matrix = NaN*ones(numel(dt),m.no_of_half_sarcomeres);
sim.f_overlap = NaN_matrix;
sim.f_activated = NaN_matrix;
sim.f_bound = NaN_matrix;
sim.hs_force = NaN_matrix;
sim.cb_force = NaN_matrix;
sim.pas_force = NaN_matrix;
sim.hs_length = NaN_matrix;
sim.Ca = NaN_matrix;
sim.M1 = NaN_matrix;
sim.M2 = NaN_matrix;
sim.M3 = NaN_matrix;
simCa = NaN_matrix;
simV = NaN_matrix;
sim.pops = NaN*ones(numel(dt),m.no_of_half_sarcomeres, ...
    sim.myofilaments.no_of_x_bins);
sim.EP = NaN*ones(43,numel(dt),m.no_of_half_sarcomeres);
sim.currents = NaN*ones(6,numel(dt),m.no_of_half_sarcomeres);
sim.LatticeSpacing = NaN*ones(1,numel(dt),m.no_of_half_sarcomeres);

% Loop along simulation with a progress bar
beats = 1;
for k = 1:beats
    for i=1:numel(dt)
        t = sum(dt(1:i));

        % Implement the time-step
        m.implement_time_step(dt(i),dml(i));
        
        if k == beats
            % Store results in a structure called sim
            sim.muscle_force(i) = m.muscle_force;
            sim.muscle_length(i) = m.muscle_length;
            for j=1:m.no_of_half_sarcomeres
                sim.f_overlap(i,j) = m.hs(j).f_overlap;
                sim.f_activated(i,j) = m.hs(j).f_on;
                sim.f_bound(i,j) = m.hs(j).f_bound;
                sim.hs_force(i,j) = m.hs(j).hs_force;
                sim.cb_force(i,j) = m.hs(j).cb_force;
                sim.pas_force(i,j) = m.hs(j).passive_force;
                sim.hs_length(i,j) = m.hs(j).hs_length;
                %         sim.Ca(i,j) = m.hs(j).Ca;
                sim.Ca(i,j) = m.hs(j).initialCondEP(39);
                sim.M1(i,j) = m.hs(j).state_pops.M1;
                sim.M2(i,j) = m.hs(j).state_pops.M2;
                sim.M3(i,j) = m.hs(j).state_pops.M3;
                % Pull out the bin distributions, which need an extra dimesion
                x_indices = 2+(1:m.hs(1).myofilaments.no_of_x_bins);
                sim.pops(i,j,:) = m.hs(j).myofilaments.y(x_indices);
                simCa(i,j) = [m.hs(j).initialCondEP(39)];
                simV(i,j) =[m.hs(j).initialCondEP(1)];
                sim.EP(:,i,j) = m.hs(j).initialCondEP;
                sim.currents(:,i,j) = m.hs(j).currents;
            end
        end

        %         figure(1);
        %         hold on
        %         subplot(2,1,1);hold on;plot(simCa.*1000)
        %         subplot(2,1,2);hold on;plot(simV)
        % Draw as we go
        %     if ((mod(i,drawing_skip)==0) || (i==numel(dt)))
        %         if (~exist('sp'))
        %             addpath('/Users/viviane/Dropbox/Macy/Campbell/matlab_myosim/original/utilities');
        %             sp = initialise_publication_quality_figure( ...
        %                 'no_of_panels_wide',2, ...
        %                 'no_of_panels_high',4, ...
        %                 'right_margin',1, ...
        %                 'x_to_y_axes_ratio',2, ...
        %                 'axes_padding_top',0.7, ...
        %                 'axes_padding_bottom',0.3, ...
        %                 'axes_padding_left',1, ...
        %                 'axes_padding_right',0.5);
        %         end
        %
        %         subplot(sp(1));
        %         cla;
        %         plot(sim.time_s,sim.muscle_length,'g-', ...
        %                 'LineWidth',trace_line_width);
        %         xlim([0 max(sim.time_s)]);
        %         ylabel('Muscle length');
        %
        %         subplot(sp(3));
        %         cla
        %         plot(sim.time_s,sim.muscle_force,'g-', ...
        %                 'LineWidth',trace_line_width);
        %         xlim([0 max(sim.time_s)]);
        %         ylabel('Muscle force');
        %
        %         subplot(sp(5));
        %         cla
        %         hold on;
        %         for j=1:m.no_of_half_sarcomeres
        %             plot(sim.time_s,sim.hs_force(:,j),'-','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %             plot(sim.time_s,sim.cb_force(:,j),':','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %             plot(sim.time_s,sim.pas_force(:,j),'--','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %         end
        %         ylabel('Half-sarcomere forces');
        %         xlim([0 max(sim.time_s)]);
        %
        %         subplot(sp(7));
        %         cla
        %         hold on
        %         for j=1:m.no_of_half_sarcomeres
        %             plot(sim.time_s,sim.hs_length(:,j),'-','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %         end
        %         ylabel('Half-sarcomere lengths');
        %         xlim([0 max(sim.time_s)]);
        %
        %         subplot(sp(2));
        %         cla;
        %         hold on;
        %         for j=1:m.no_of_half_sarcomeres
        %             plot(sim.time_s,sim.f_overlap(:,j),':','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %             plot(sim.time_s,sim.f_activated(:,j),'-','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %             plot(sim.time_s,sim.f_bound(:,j),'--','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %         end
        %         title({'Dotted - overlap','Solid - activated','Dashed - bound'});
        %         xlim([0 max(sim.time_s)]);
        %
        %         subplot(sp(4));
        %         cla
        %         hold on;
        %         for j=1:m.no_of_half_sarcomeres
        %             plot(sim.time_s,-log10(simCa(:,j)),'-','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %         end
        %         ylabel('pCa');
        %         xlim([0 max(sim.time_s)]);
        %
        %         subplot(sp(6));
        %         cla
        %         hold on;
        %         for j=1:m.no_of_half_sarcomeres
        %             plot(sim.time_s,sim.M1(:,j),':','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %             plot(sim.time_s,sim.M2(:,j),'-','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %             plot(sim.time_s,sim.M3(:,j),'--','Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %         end
        %         title({'Dotted - M1','Solid - M2','Dashed - M3'});
        %         xlim([0 max(sim.time_s)]);
        %
        %         subplot(sp(8));
        %         cla
        %         hold on;
        %         for j=1:m.no_of_half_sarcomeres
        %             plot(sim.myofilaments.x,squeeze(sim.pops(i,j,:)),'-', ...
        %                 'Color',cm(j,:), ...
        %                 'LineWidth',trace_line_width);
        %         end
        %         ylabel('M3 distributions');
        %         xlim([min(sim.myofilaments.x) max(sim.myofilaments.x)]);
        %         ylim([0 0.05]);


        %         figure(2);
        %         hold on
        %         subplot(2,1,1);hold on;plot(sim.time_s,simCa.*1000)
        %         subplot(2,1,2);hold on;plot(sim.time_s,simV)
        %
        %         drawnow;
        %
        %         if (~isempty(image_dump_folder))
        %             output_file_string = ...
        %                 fullfile(image_dump_folder,sprintf('frame_%.0f',i));
        %             print(output_file_string,'-dpng','-r150');
        %         end
    end
    for itime = 1:m.no_of_half_sarcomeres
        m.hs(itime).time = 0;
    end
end
save Strongly_10
end
