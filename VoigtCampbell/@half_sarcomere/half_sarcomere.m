classdef half_sarcomere < handle
    
    properties
        % These are properties that can be accessed from outside the
        % half-sarcomere class
        
        hs_length = 1082;   % the length of the half-sarcomere in nm
        hs_force;           % the stress (in N m^(-2)) in the half-sarcomere

        f_overlap;
        f_on;
        f_bound;
        
        cb_force;
        passive_force;
        
        state_pops;
        
%         Ca = 10^(-9.0);     % Ca concentration (in M)
        
        kinetic_scheme = '3state_with_SRX';
        
        check_force = 0;    % temp force used during force balance
        
        myofilaments = [];  % a structure that holds the data for
                            % half-sarcomere kinetics
                            % precise format depends on the kinetic scheme
                            
        parameters = [];    % a structure that holds the model parameters
                            % precise format depends on the kinetic scheme

        % EP model
        parametersEP = [];
        initialCondEP = [];
        EPoutput = [];
        segments = muscle.no_of_half_sarcomeres;
        time = 0;
        currents = [];
    end
    
    properties (SetAccess = private)
        % These are properties that can only be accessed from within
        % the half-sarcomere class
                           
    end
    
    methods
        
        % Constructor
        function obj = half_sarcomere(varargin)
            
            % Set up kinetic scheme
            switch (obj.kinetic_scheme)
                
                case '3state_with_SRX';
                    % 3 state model SRX model described by Campbell,
                    % et al. 2018
                    obj.myofilaments.bin_min = -10;
                            % min x value for myosin distributions in nm
                    obj.myofilaments.bin_max = 10;
                            % max x value for myosin distributions in nm
                    obj.myofilaments.bin_width = 0.5;
                            % width of bins for myosin distributions in nm
                    obj.myofilaments.x = ...
                        obj.myofilaments.bin_min : ...
                            obj.myofilaments.bin_width : ...
                                obj.myofilaments.bin_max;
                            % array of x_bin values
                    obj.myofilaments.no_of_x_bins = ...
                        numel(obj.myofilaments.x);
                            % no of x_bins
                    
                    obj.myofilaments.thick_filament_length = 815;
                            % length of thick filaments in nm
                    obj.myofilaments.thin_filament_length = 1120;
                            % length of thin filaments in nm
                    obj.myofilaments.bare_zone_length = 80;
                            % length of thick filament bare zone in nm
                    obj.myofilaments.k_falloff = 0.0024;
                            % defines how f_overlap falls hs_length shortens
                            % below optimal
                    
                    % Set up the y vector which is used for the kinetics
                    obj.myofilaments.y_length = ...
                        obj.myofilaments.no_of_x_bins + 4;
                    obj.myofilaments.y = ...
                        zeros(obj.myofilaments.y_length,1);
                    
                    % Start with all the cross-bridges in M1 and all
                    % binding sites off
                    obj.myofilaments.y(1)=1.0;
                    obj.myofilaments.y(end-1) = 1.0;
                    
                    % Set parameters
                    obj.parameters.k_1 = 1.83;
                    obj.parameters.k_force = 2.34e-4;
                    obj.parameters.k_2 = 100;
                    obj.parameters.k_3 = 3820./100;
                    obj.parameters.k_4_0 = 3610./100;
                    obj.parameters.k_4_1 = 0.216./100;
                    obj.parameters.k_cb = 0.001;
                    obj.parameters.x_ps = 3.18;
                    obj.parameters.k_on = 2.59e8./20;
                    obj.parameters.k_off = 200*10;
                    obj.parameters.k_coop = 5.64;
                    
                    obj.parameters.passive_force_mode = 'exponential';
                    obj.parameters.passive_sigma = 5.67e-12;
                    obj.parameters.passive_L = 31.7;

                    obj.parameters.compliance_factor = 0.5;
                            % proportion of delta_hsl that the
                            % cb distribution is moved
                    obj.parameters.cb_number_density = 6.9e16;
                            % number of cbs in a half-sarcomere with a
                            % cross-sectional area of 1 m^2
                            
                    obj.parameters.k_boltzmann = 1.38e-23;
                            % Boltzmann's constant (J K^-1)
                    obj.parameters.temperature = 288;
                            % Temperature (K)
                            
                    obj.parameters.max_rate = 5000;
                            % Max rate (in s^-1) allowed in ODEs
                otherwise
                    error(sprintf( ...
                        '%s kinetics scheme not yet implemented in half_sarcomere class', ...
                        obj.kinetic_scheme));
            end
            
            % Initialise forces
            obj.cb_force = 0;
            obj.passive_force = return_passive_force(obj,obj.hs_length);
            obj.hs_force = obj.cb_force + obj.passive_force;

            % Intialise_populations
            obj.f_on = 0;
            obj.f_bound = 0;
            
            
            
            % EP Model Parameters
            obj.parametersEP.R = 8314;       % [J./kmol.*K]
            obj.parametersEP.Frdy = 96485;   % [C./mol]
            obj.parametersEP.Temp = 310;     % [K]
            obj.parametersEP.FoRT = obj.parametersEP.Frdy./obj.parametersEP.R./obj.parametersEP.Temp;
            obj.parametersEP.Cmem = 1.3810e-10./obj.segments;   % [F] membrane capacitance
            obj.parametersEP.Qpow = (obj.parametersEP.Temp-310)./10;
            
            % Cell geometry
            obj.parametersEP.cellLength = 100./obj.segments;     % cell length [um]
            obj.parametersEP.cellRadius = 10.25;   % cell radius [um]
            obj.parametersEP.Vcell = pi.*obj.parametersEP.cellRadius.^2.*obj.parametersEP.cellLength.*1e-15;    % [L]
            obj.parametersEP.Vmyo = 0.65.*obj.parametersEP.Vcell;
            obj.parametersEP.Vsr = 0.035.*obj.parametersEP.Vcell;
            obj.parametersEP.Vsl = 0.02.*obj.parametersEP.Vcell;
            obj.parametersEP.Vjunc = 0.0539.*.01.*obj.parametersEP.Vcell;
            
            obj.parametersEP.J_ca_juncsl = 1./1.2134e12./obj.segments; % [L./msec] = 8.2413e-13 
            obj.parametersEP.J_ca_slmyo = 1./2.68510e11./obj.segments; % [L./msec] = 3.2743e-12
            obj.parametersEP.J_na_juncsl = 1./(1.6382e12./3.*100)./obj.segments; % [L./msec] = 6.1043e-13
            obj.parametersEP.J_na_slmyo = 1./(1.8308e10./3.*100)./obj.segments;  % [L./msec] = 5.4621e-11
 
            
            % Fractional currents in compartments
            obj.parametersEP.Fjunc = 0.11;
            obj.parametersEP.Fsl = 1-obj.parametersEP.Fjunc;
            obj.parametersEP.Fjunc_CaL = 0.9;
            obj.parametersEP.Fsl_CaL = 1-obj.parametersEP.Fjunc_CaL;
            
            % Fixed ion concentrations
            obj.parametersEP.Cli = 15;   % Intracellular Cl  [mM]
            obj.parametersEP.Clo = 150;  % Extracellular Cl  [mM]
            obj.parametersEP.Ko = 5.4;   % Extracellular K   [mM]
            obj.parametersEP.Nao = 140;  % Extracellular Na  [mM]
            obj.parametersEP.Cao = 1.8;  % Extracellular Ca  [mM]
            obj.parametersEP.Mgi = 1;    % Intracellular Mg  [mM]
            
            % Na transport parameters
            
            obj.parametersEP.GNa=16;
            obj.parametersEP.GNaB = 0.297e-3;    % [mS./uF]
            obj.parametersEP.IbarNaK = 1.90719;     % [uA./uF]
            obj.parametersEP.KmNaip = 11;         % [mM]
            obj.parametersEP.KmKo = 1.5;         % [mM]
            obj.parametersEP.Q10NaK = 1.63;
            obj.parametersEP.Q10KmNai = 1.39;
            
            %% K current parameters
            obj.parametersEP.pNaK = 0.01833;
            obj.parametersEP.GtoSlow = 0.06.*1;     % [mS./uF] %0.09 CaMKII
            obj.parametersEP.GtoFast = 0.02.*1;     % [mS./uF]
            obj.parametersEP.gkp = 0.001;
            
            % Cl current parameters
            obj.parametersEP.GClCa = 0.109625;   % [mS./uF]
            obj.parametersEP.GClB = 9e-3;        % [mS./uF]
            obj.parametersEP.KdClCa = 100e-3;    % [mM]
            
            % I_Ca parameters
            obj.parametersEP.pNa = 1.5e-8;       % [cm./sec]
            obj.parametersEP.pCa = 5.4e-4;       % [cm./sec]
            obj.parametersEP.pK = 2.7e-7;        % [cm./sec]
            obj.parametersEP.Q10CaL = 1.8;
            
            %Ca transport parameters
            obj.parametersEP.IbarNCX = 9.0;      % [uA./uF]
            obj.parametersEP.KmCai = 3.59e-3;    % [mM]
            obj.parametersEP.KmCao = 1.3;        % [mM]
            obj.parametersEP.KmNai = 12.29;      % [mM]
            obj.parametersEP.KmNao = 87.5;       % [mM]
            obj.parametersEP.ksat = 0.27;        % [none]
            obj.parametersEP.nu = 0.35;          % [none]
            obj.parametersEP.Kdact = 0.256e-3;   % [mM]
            obj.parametersEP.Q10NCX = 1.57;      % [none]
            obj.parametersEP.IbarSLCaP = 0.0673; % [uA./uF](2.2 umol./L cytosol./sec)
            obj.parametersEP.KmPCa = 0.5e-3;     % [mM]
            obj.parametersEP.GCaB = 2.513e-4;    % [uA./uF]
            obj.parametersEP.Q10SLCaP = 2.35;    % [none]
            
            %SR flux parameters
            obj.parametersEP.Q10SRCaP = 2.6;          % [none]
            obj.parametersEP.Vmax_SRCaP = 5.3114e-3;  % [mM./msec] (286 umol./L cytosol./sec)
            obj.parametersEP.Vmax_SRCaP = obj.parametersEP.Vmax_SRCaP; %converrting from ms to sec
            obj.parametersEP.Kmf = 0.246e-3;          % [mM]
            obj.parametersEP.Kmr = 1.7;               % [mM]L cytosol
            obj.parametersEP.hillSRCaP = 1.787;       % [mM]
            obj.parametersEP.ks = 25;                 % [1./ms]
            obj.parametersEP.ks = obj.parametersEP.ks ; % converting from [1./ms] to [1./ms]
            obj.parametersEP.koCa = 10;               % [mM.^-2 1./ms]   %default 10   modified 20
            obj.parametersEP.kom = 0.06;              % [1./ms]
            obj.parametersEP.kiCa = 0.5;              % [1./mM./ms]
            obj.parametersEP.kim = 0.005;             % [1./ms]
            obj.parametersEP.ec50SR = 0.45;           % [mM]
            
            %Buffering parameters
            obj.parametersEP.Bmax_Naj = 7.561;       % [mM]  % Na buffering
            obj.parametersEP.Bmax_Nasl = 1.65;       % [mM]
            obj.parametersEP.koff_na = 1e-3;         % [1./ms]
            obj.parametersEP.kon_na = 0.1e-3;        % [1./mM./ms]
            obj.parametersEP.Bmax_TnClow = 70e-3;    % [mM]                      % TnC low affinity
            obj.parametersEP.koff_tncl = 19.6e-3;    % [1./ms]
            obj.parametersEP.kon_tncl = 32.7;        % [1./mM./ms]
            obj.parametersEP.Bmax_TnChigh = 140e-3;  % [mM]                      % TnC high affinity
            obj.parametersEP.koff_tnchca = 0.032e-3; % [1./ms]
            obj.parametersEP.kon_tnchca = 2.37;      % [1./mM./ms]
            obj.parametersEP.koff_tnchmg = 3.33e-3;  % [1./ms]
            obj.parametersEP.kon_tnchmg = 3e-3;      % [1./mM./ms]
            obj.parametersEP.Bmax_CaM = 24e-3;       % [mM]  % CaM buffering
            obj.parametersEP.koff_cam = 238e-3;      % [1./ms]
            obj.parametersEP.kon_cam = 34;           % [1./mM./ms]
            obj.parametersEP.Bmax_myosin = 140e-3;   % [mM]                      % Myosin buffering
            obj.parametersEP.koff_myoca = 0.46e-3;   % [1./ms]
            obj.parametersEP.kon_myoca = 13.8;       % [1./mM./ms]
            obj.parametersEP.koff_myomg = 0.057e-3;  % [1./ms]
            obj.parametersEP.kon_myomg = 0.0157;     % [1./mM./ms]
            obj.parametersEP.Bmax_SR = 19.*.9e-3;     % [mM] (Bers text says 47e-3) 19e-3
            obj.parametersEP.koff_sr = 60e-3;        % [1./ms]
            obj.parametersEP.kon_sr = 100;           % [1./mM./ms]
            obj.parametersEP.Bmax_SLlowsl = 37.4e-3.*obj.parametersEP.Vmyo./obj.parametersEP.Vsl;        % [mM]    % SL buffering
            obj.parametersEP.Bmax_SLlowj = 4.6e-3.*obj.parametersEP.Vmyo./obj.parametersEP.Vjunc.*0.1;    % [mM]
            obj.parametersEP.koff_sll = 1300e-3;     % [1./ms]
            obj.parametersEP.kon_sll = 100;          % [1./mM./ms]
            obj.parametersEP.Bmax_SLhighsl = 13.4e-3.*obj.parametersEP.Vmyo./obj.parametersEP.Vsl;       % [mM]
            obj.parametersEP.Bmax_SLhighj = 1.65e-3.*obj.parametersEP.Vmyo./obj.parametersEP.Vjunc.*0.1;  % [mM]
            obj.parametersEP.koff_slh = 30e-3;       % [1./ms]
            obj.parametersEP.kon_slh = 100;          % [1./mM./ms]
            obj.parametersEP.Bmax_Csqn = 140e-3.*obj.parametersEP.Vmyo./obj.parametersEP.Vsr;            % [mM] % Bmax_Csqn = 2.6;      % Csqn buffering
            obj.parametersEP.koff_csqn = 65;         % [1./ms]
            obj.parametersEP.kon_csqn = 100;         % [1./mM./ms]
            obj.parametersEP.lattice_0 = 25.3;
            
            %% calcium buffering on TnC from myofilment model
            TnCL = 0.0147730085063734.*10^-6; %convert from millimol to M
            TnCH = 0.13066096561522.*10^-6; %convert from millimol to M
            M3EP = 1e-4;
            
            %% Initial conditions
            mo=1.405627e-3;
            ho= 9.867005e-1;
            jo=9.915620e-1; 
            do=7.175662e-6; 
            fo=1.000681; 
            fcaBjo=2.421991e-2;
            fcaBslo=1.452605e-2;
            xtoso=4.051574e-3;
            ytoso=9.945511e-1; 
            xtofo=4.051574e-3; 
            ytofo= 9.945511e-1; 
            xkro=8.641386e-3; 
            xkso= 5.412034e-3;
            RyRro=8.884332e-1;
            RyRoo=8.156628e-7; 
            RyRio=1.024274e-7; 
            NaBjo=3.539892;
            NaBslo=7.720854e-1; 
            TnClo=8.773191e-3; 
            TnCHco=1.078283e-1; 
            TnCHmo=1.524002e-2; 
            CaMo=2.911916e-4; 
            Myoco=1.298754e-3; 
            Myomo=1.381982e-1;
            SRBo=2.143165e-3; 
            SLLjo=9.566355e-3; 
            SLLslo=1.110363e-1; 
            SLHjo=7.347888e-3; 
            SLHslo=7.297378e-2; 
            Csqnbo= 1.242988;
            Ca_sro=5.545201e-1; %0.9 %% changed
            Najo=8.80329; 
            Naslo=8.80733; 
            Naio=8.80853; 
            Kio=135; 
            Cajo=1.737475e-4; 
            Caslo= 1.031812e-4; 
            Caio=8.597401e-5; 
            Vmo=-8.556885e+1; 
            rtoso=0.9946;

            %%
            % Gating variables   
            y00 = Vmo;
            %   1       2       3       4       5       6       7       8       9       10      11      12      13
            %%   m       h       j       d       f       fcaBj   fcaBsl   xtos    ytos    xtof    ytof    xkr     xks   
            %y10=[1.2e-3;0.99;   0.99;   0.0;    1.0;    0.0141; 0.0141;     0;      1;      0.0;    1.0;    0.0;    6e-3;];
            y10=[mo; ho; jo; do; fo; fcaBjo; fcaBslo; xtoso; ytoso; xtofo; ytofo; xkro; xkso;];  
            % RyR and Buffering variables
            %   14      15      16      17      18      19      20      21      22      23      24
            %%   RyRr    RyRo    RyRi    NaBj    NaBsl   TnCL    TnCHc   TnCHm   CaM     Myoc    Myom  
            y20=[RyRro; RyRoo; RyRio; NaBjo; NaBslo; TnClo; TnCHco; TnCHmo; CaMo; Myoco; Myomo;];     
            %y20=[1;     0;      0;      1.8;   0.8;    0.012;   0.112;  0.01;   0.4e-3; 1.9e-3; 0.135;];
            % More buffering variables
            %   25      26      27      28      29      30
            %%   SRB     SLLj   SLLsl    SLHj    SLHsl  Csqnb
            y30=[SRBo; SLLjo; SLLslo; SLHjo; SLHslo; Csqnbo];
            %y30=[3.3e-3; 0.012; 0.012; 0.13;  0.13;  1.5;];
            %   Intracellular concentrations./ Membrane voltage
            %    31      32      33      34      35      36      37     38       39    40    
            %%    Ca_sr   Naj     Nasl    Nai     Ki      Caj     Casl    Cai     Vm    rtos  
            y40=[Ca_sro; Najo; Naslo; Naio; Kio; Cajo; Caslo; Caio; rtoso; ];    
            %y40=[0.9;    8.8;    8.8;    8.8;    135;    0.1e-3; 0.1e-3; 0.1e-3; -88;  0.89; 0;          0;];

            %% TnCL TnCH M3EP
            y50 = [TnCL ; TnCH ; M3EP];
            %% Put everything together
            obj.initialCondEP= [y00;y10;y20;y30;y40;y50];
            
%             %% load file for initial conditions
%             load('stronly_61.mat','sim')

        end
        
        % Other methods
        f_overlap = return_f_overlap(obj);
        pf = return_passive_force(obj,hsl);
        
        evolve_kinetics(obj,time_step,lattice);
        update_3state_with_SRX(obj,time_step,lattice);
        
        move_cb_distribution(obj,delta_hsl);
        update_forces(obj);
        
        check_new_force(obj,new_length);

        implement_time_step(obj,time_step,delta_hsl,lattice);
        
%         calcurrents(obj,time_step,initialCondEP);
        
    end
end
            
            
            
            
            
        
        