function update_3state_with_SRC(obj,time_step,lattice);
% Function updates kinetics for thick and thin filaments based on 3 state
% SRX model descripbed by Campbell et al, 2018

% Pull out the myofilaments vector
y = obj.myofilaments.y;

% Get the overlap
N_overlap = return_f_overlap(obj);

%EP
y0 = [obj.initialCondEP(:,end)];

% Evolve the system

options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','on');
% [t,y] = ode15s(@f,[obj.time-timestep,obj.time],y0,options,obj,hs_counter,0);
[t,y_new] = ode15s(@derivs,[obj.time-time_step,obj.time],[y;y0],options);

% Update the system
obj.myofilaments.y = y_new(end,1:end-43)';
obj.f_overlap = N_overlap;
obj.f_on = obj.myofilaments.y(end);
obj.f_bound = sum(obj.myofilaments.y(2+(1:obj.myofilaments.no_of_x_bins)));

%update the EP system
obj.EPoutput = y_new(end,end-43+1:end)';
% obj.initialCondEP = y_new(end,end-40+1:end)';
 
%update currents
currents = calcCurrents(t,y_new);
obj.currents = currents(end,:);

%% Nested function
    function dy = derivs(time_step,z,runType)
        
        y = z(1:end-43);
        yEP = z(end-43+1:end);
        
        Ca_i = yEP(39).*1000;
        
        %% change in myosin head attachment due to lattice spacing 
        k_3_changed = obj.parameters.k_3 - ((lattice - obj.parametersEP.lattice_0) ).* ((obj.parametersEP.lattice_0 - lattice)>0);

        %% Set dy
        dy = zeros(numel(y),1);
        
        % Unpack
        M1 = y(1);
        M2 = y(2);
        M3 = y(2+(1:obj.myofilaments.no_of_x_bins));
        N_off = y(end-1);
        N_on = y(end);
        N_bound = sum(M3);
        
        % Calculate the fluxes
        r1 = min([obj.parameters.max_rate ...
            obj.parameters.k_1 * ...
            (1+(obj.parameters.k_force * obj.hs_force))]);
        J1 = r1 * M1;
        
        r2 = min([obj.parameters.max_rate obj.parameters.k_2]);
        J2 = r2 * M2;
        
        r3 = k_3_changed * ...
            exp(-obj.parameters.k_cb * (obj.myofilaments.x).^2 / ...
            (2 * 1e18 * obj.parameters.k_boltzmann * ...
            obj.parameters.temperature));
        r3(r3>obj.parameters.max_rate)=obj.parameters.max_rate;
        
        J3 = r3 .* obj.myofilaments.bin_width * M2 * (N_on - N_bound);
        
        r4 = obj.parameters.k_4_0 + ...
            (obj.parameters.k_4_1 * ...
            ((obj.myofilaments.x - obj.parameters.x_ps).^4));
        r4(r4>obj.parameters.max_rate)=obj.parameters.max_rate;
        J4 = r4 .* M3';
        
        J_on = obj.parameters.k_on * Ca_i * (N_overlap - N_on) * ...
            (1 + obj.parameters.k_coop * (N_on/N_overlap));
        
        J_off = obj.parameters.k_off * (N_on - N_bound) * ...
            (1 + obj.parameters.k_coop * ((N_overlap - N_on)/N_overlap));
        
        % Calculate the derivs
        dy(1) = -J1 + J2;
        dy(2) = (J1 + sum(J4)) - (J2 + sum(J3));
        for i=1:obj.myofilaments.no_of_x_bins
            dy(2+i) = J3(i) - J4(i);
        end
        dy(end-1) = -J_on + J_off;
        dy(end) = J_on - J_off;
        
        %% EP model
        ydot = zeros(size(yEP));
        
        %% input values
        Vmo             = yEP(1);
        
        mo                 = yEP(2);
        ho                 = yEP(3);
        jo                 = yEP(4);
        do                 = yEP(5);
        fo                 = yEP(6);
        fcaBjo             = yEP(7);
        fcaBslo            = yEP(8);
        xtoso              = yEP(9);
        ytoso              = yEP(10);
        xtofo              = yEP(11);
        ytofo              = yEP(12);
        xkro               = yEP(13);
        xkso               = yEP(14);
        RyRro              = yEP(15);
        RyRoo              = yEP(16);
        RyRio              = yEP(17);
        NaBjo              = yEP(18);
        NaBslo             = yEP(19);
        TnClo              = yEP(20);
        TnCHco             = yEP(21);
        TnCHmo             = yEP(22);
        CaMo               = yEP(23);
        Myoco              = yEP(24);
        Myomo              = yEP(25);
        SRBo               = yEP(26);
        SLLjo              = yEP(27);
        SLLslo             = yEP(28);
        SLHjo              = yEP(29);
        SLHslo             = yEP(30);
        Csqnbo             = yEP(31);
        Ca_sro             = yEP(32);
        Najo               = yEP(33);
        Naslo              = yEP(34);
        Naio               = yEP(35);
        Kio                = yEP(36);
        Cajo               = yEP(37);
        Caslo              = yEP(38);
        Caio               = yEP(39);
        rtoso              = yEP(40);
        TnCL               = yEP(41);
        TnCH               = yEP(42);
        M3EP               = yEP(43);
        
        %% new version of TnC buffering
        dM2EP = dy(2);
        dM3EP = 0;
        for i=1:obj.myofilaments.no_of_x_bins
            dM3EP = dM3EP + dy(2+i);
        end
        
        
        % on and off-rate from Rice model
        kon         = 50e-3*1e-6 * 1000./1.5;        % (1./[ms uM]) % e-6 for um to M % 1000 ms to s
        koffL       = 250e-3*1000*1.5;              % (1./ms) % 1000 ms to s
        koffH       = 25e-3*1000*1.5;               % (1./ms) % 1000 ms to s
        Trop_conc   = 70;                       % (uM) troponin concentration
        
        
        dTnCL = kon .* Ca_i * (1-TnCL) - koffL .* TnCL;
        dTnCH = kon .* Ca_i * (1-TnCH) - koffH .* TnCH;
        
        
        
        % steady-state fractions in XBprer and XBpostr using King-Altman rule
        SSM2 = (obj.parameters.k_1 * (obj.parameters.k_4_0))/...
                (obj.parameters.k_1 * k_3_changed + obj.parameters.k_1 * (obj.parameters.k_4_0) + obj.parameters.k_2 * (obj.parameters.k_4_0));
        SSM3 = obj.parameters.k_1 * k_3_changed/...
                (obj.parameters.k_1 * k_3_changed + obj.parameters.k_1 * (obj.parameters.k_4_0) + obj.parameters.k_2 * (obj.parameters.k_4_0));
            
        % Ca buffering by low-affinity troponin C (LTRPNCa)
        FrSBXB    = (M2+M3EP)/(SSM2 + SSM3);
        dFrSBXB   = (dM2EP+dM3EP)/(SSM2 + SSM3);
        
        TnCTot = Trop_conc*((1-N_overlap)*TnCL + ...
          N_overlap*(FrSBXB*TnCH+(1-FrSBXB)*TnCL));
        dTnCTot= Trop_conc*((1-N_overlap)*dTnCL + ...
          N_overlap*(dFrSBXB*TnCH+FrSBXB*dTnCH-dFrSBXB*TnCL+...
          (1-FrSBXB)*dTnCL));
      
        %%
        % Nernst Potentials
        obj.parametersEP.ena_junc = (1./obj.parametersEP.FoRT).*log(obj.parametersEP.Nao./Najo);     % [mV]
        obj.parametersEP.ena_sl = (1./obj.parametersEP.FoRT).*log(obj.parametersEP.Nao./Naslo);       % [mV]
        obj.parametersEP.ek = (1./obj.parametersEP.FoRT).*log(obj.parametersEP.Ko./Kio);	        % [mV]
        obj.parametersEP.eca_junc = (1./obj.parametersEP.FoRT./2).*log(obj.parametersEP.Cao./Cajo);   % [mV]
        obj.parametersEP.eca_sl = (1./obj.parametersEP.FoRT./2).*log(obj.parametersEP.Cao./Caslo);     % [mV]
        obj.parametersEP.ecl = (1./obj.parametersEP.FoRT).*log(obj.parametersEP.Cli./obj.parametersEP.Clo);            % [mV]
        
        
        %% Membrane Currents
        % I_Na: Fast Na Current
        am = 0.32.*(Vmo+47.13)./(1-exp(-0.1.*(Vmo+47.13)));
        bm = 0.08.*exp(-Vmo./11);
        if Vmo >= -40
            ah = 0; aj = 0;
            bh = 1./(0.13.*(1+exp(-(Vmo+10.66)./11.1)));
            bj = 0.3.*exp(-2.535e-7.*Vmo)./(1+exp(-0.1.*(Vmo+32)));
        else
            ah = 0.135.*exp((80+Vmo)./-6.8);
            bh = 3.56.*exp(0.079.*Vmo)+3.1e5.*exp(0.35.*Vmo);
            aj = (-1.2714e5.*exp(0.2444.*Vmo)-3.474e-5.*exp(-0.04391.*Vmo)).*(Vmo+37.78)./(1+exp(0.311.*(Vmo+79.23)));
            bj = 0.1212.*exp(-0.01052.*Vmo)./(1+exp(-0.1378.*(Vmo+40.14)));
        end
        dmo = am.*(1-mo)-bm.*mo;
        dho = ah.*(1-ho)-bh.*ho;
        djo = aj.*(1-jo)-bj.*jo;
        
        
        I_Na_junc = obj.parametersEP.Fjunc.*obj.parametersEP.GNa.*mo.^3.*ho.*jo.*(Vmo-obj.parametersEP.ena_junc);
        I_Na_sl = obj.parametersEP.Fsl.*obj.parametersEP.GNa.*mo.^3.*ho.*jo.*(Vmo-obj.parametersEP.ena_sl);
        
        % I_Na_junc= I_Na_junc1.*(1-flag)+I_Na_junc2.*flag;
        % I_Na_sl= I_Na_sl1.*(1-flag)+I_Na_sl2.*flag;
        I_Na = I_Na_junc+I_Na_sl;
        
        
        
        % I_nabk: Na Background Current
        I_nabk_junc = obj.parametersEP.Fjunc.*obj.parametersEP.GNaB.*(Vmo-obj.parametersEP.ena_junc);
        I_nabk_sl = obj.parametersEP.Fsl.*obj.parametersEP.GNaB.*(Vmo-obj.parametersEP.ena_sl);
        I_nabk = I_nabk_junc+I_nabk_sl;
        
        % I_nak: Na./K Pump Current
        sigma = (exp(obj.parametersEP.Nao./67.3)-1)./7;
        fnak = 1./(1+0.1245.*exp(-0.1.*Vmo.*obj.parametersEP.FoRT)+0.0365.*sigma.*exp(-Vmo.*obj.parametersEP.FoRT));
        I_nak_junc = obj.parametersEP.Fjunc.*obj.parametersEP.IbarNaK.*fnak.*obj.parametersEP.Ko ./(1+(obj.parametersEP.KmNaip./Najo).^4) ./(obj.parametersEP.Ko+obj.parametersEP.KmKo);
        I_nak_sl = obj.parametersEP.Fsl.*obj.parametersEP.IbarNaK.*fnak.*obj.parametersEP.Ko ./(1+(obj.parametersEP.KmNaip./Naslo).^4) ./(obj.parametersEP.Ko+obj.parametersEP.KmKo);
        I_nak = I_nak_junc+I_nak_sl;
        
        % I_kr: Rapidly Activating K Current
        gkr = 0.03.*sqrt(obj.parametersEP.Ko./5.4);
        xrss = 1./(1+exp(-(Vmo+50)./7.5));
        tauxr = 1./(1.38e-3.*(Vmo+7)./(1-exp(-0.123.*(Vmo+7)))+6.1e-4.*(Vmo+10)./(exp(0.145.*(Vmo+10))-1));
        dxkro = (xrss-xkro)./tauxr;
        rkr = 1./(1+exp((Vmo+33)./22.4));
        I_kr = gkr.*xkro.*rkr.*(Vmo-obj.parametersEP.ek);
        
        % I_obj.parametersEP.ks: Slowly Activating K Current
        obj.parametersEP.pCaks_junc = -log10(Cajo)+3.0;
        obj.parametersEP.pCaks_sl = -log10(Caslo)+3.0;
        gks_junc = 0.07.*(0.057 +0.19./(1+ exp((-7.2+obj.parametersEP.pCaks_junc)./0.6)));
        gks_sl = 0.07.*(0.057 +0.19./(1+ exp((-7.2+obj.parametersEP.pCaks_sl)./0.6)));
        eks = (1./obj.parametersEP.FoRT).*log((obj.parametersEP.Ko+obj.parametersEP.pNaK.*obj.parametersEP.Nao)./(Kio+obj.parametersEP.pNaK.*Naio));
        xsss = 1./(1+exp(-(Vmo-1.5)./16.7));
        tauxs = 1./(7.19e-5.*(Vmo+30)./(1-exp(-0.148.*(Vmo+30)))+1.31e-4.*(Vmo+30)./(exp(0.0687.*(Vmo+30))-1));
        dxkso = (xsss-xkso)./tauxs;
        I_ks_junc = obj.parametersEP.Fjunc.*gks_junc.*xkso.^2.*(Vmo-eks);
        I_ks_sl = obj.parametersEP.Fsl.*gks_sl.*xkso.^2.*(Vmo-eks);
        I_ks = I_ks_junc+I_ks_sl;
        
        %I_kp: Plateau K current
        kp_kp = 1./(1+exp(7.488-Vmo./5.98));
        I_kp_junc = obj.parametersEP.Fjunc.*obj.parametersEP.gkp.*kp_kp.*(Vmo-obj.parametersEP.ek);
        I_kp_sl = obj.parametersEP.Fsl.*obj.parametersEP.gkp.*kp_kp.*(Vmo-obj.parametersEP.ek);
        I_kp = I_kp_junc+I_kp_sl;
        
        %% I_to: Transient Outward K Current (slow and fast components)
        xtoss = 1./(1+exp(-(Vmo+3.0)./15));
        ytoss = 1./(1+exp((Vmo+33.5)./10));
        rtoss = 1./(1+exp((Vmo+33.5)./10));
        tauxtos = 9./(1+exp((Vmo+3.0)./15))+0.5;
        tauytos = 3e3./(1+exp((Vmo+60.0)./10))+30;
        %tauytos = 182./(1+exp((Vmo+33.5)./10))+1;
        taurtos = 2.8e3./(1+exp((Vmo+60.0)./10))+220; %Fei changed here!! time-dependent gating variable
        %taurtos =8085./(1+exp((Vmo+33.5)./10))+313;
        dxtoso = (xtoss-xtoso)./tauxtos;
        dytoso = (ytoss-ytoso)./tauytos;
        drtoso= (rtoss-rtoso)./taurtos; %Fei changed here!! time-dependent gating variable
        I_tos = obj.parametersEP.GtoSlow.*xtoso.*(ytoso+0.5.*rtoso).*(Vmo-obj.parametersEP.ek);    % [uA./uF]
        
        tauxtof = 3.5.*exp(-Vmo.*Vmo./30./30)+1.5;
        %tauxtof = 3.5.*exp(-((Vmo+3)./30).^2)+1.5;
        tauytof = 20.0./(1+exp((Vmo+33.5)./10))+20.0;
        %tauytof = 20.0./(1+exp((Vmo+33.5)./10))+20.0;
        dxtofo = (xtoss-xtofo)./tauxtof;
        dytofo = (ytoss-ytofo)./tauytof;
        I_tof = obj.parametersEP.GtoFast.*xtofo.*ytofo.*(Vmo-obj.parametersEP.ek);
        I_to = I_tos + I_tof;
        
        % I_ki: Time-Independent K Current
        aki = 1.02./(1+exp(0.2385.*(Vmo-obj.parametersEP.ek-59.215)));
        bki =(0.49124.*exp(0.08032.*(Vmo+5.476-obj.parametersEP.ek)) + exp(0.06175.*(Vmo-obj.parametersEP.ek-594.31))) ./(1 + exp(-0.5143.*(Vmo-obj.parametersEP.ek+4.753)));
        kiss = aki./(aki+bki);
        I_ki = 0.9.*sqrt(obj.parametersEP.Ko./5.4).*kiss.*(Vmo-obj.parametersEP.ek);
        
        % I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current
        I_ClCa_junc = obj.parametersEP.Fjunc.*obj.parametersEP.GClCa./(1+obj.parametersEP.KdClCa./Cajo).*(Vmo-obj.parametersEP.ecl);
        I_ClCa_sl = obj.parametersEP.Fsl.*obj.parametersEP.GClCa./(1+obj.parametersEP.KdClCa./Caslo).*(Vmo-obj.parametersEP.ecl);
        I_ClCa = I_ClCa_junc+I_ClCa_sl;
        I_Clbk = obj.parametersEP.GClB.*(Vmo-obj.parametersEP.ecl);
        
        %% I_Ca: L-type Calcium Current
        dss = 1./(1+exp(-(Vmo+14.5)./6.0));
        taud = dss.*(1-exp(-(Vmo+14.5)./6.0))./(0.035.*(Vmo+14.5));
        fss = 1./(1+exp((Vmo+35.06)./3.6))+0.6./(1+exp((50-Vmo)./20));
        tauf = 1./(0.0197.*exp( -(0.0337.*(Vmo+14.5)).^2 )+0.02);
        ddo = (dss-do)./taud;
        dfo = (fss-fo)./tauf;
        dfcaBjo = 1.7.*Cajo.*(1-fcaBjo)-11.9e-3.*fcaBjo; % fCa_junc   obj.parametersEP.Koff!!!!!!!!
        dfcaBslo = 1.7.*Caslo.*(1-fcaBslo)-11.9e-3.*fcaBslo; % fCa_sl
        fcaCaMSL= 0.1./(1+(0.01./Caslo));
        fcaCaj= 0.1./(1+(0.01./Cajo));
        fcaCaMSL=0;
        fcaCaj= 0;
        %fcaBjo=0;
        %fcaBslo=0;
        ibarca_j = obj.parametersEP.pCa.*4.*(Vmo.*obj.parametersEP.Frdy.*obj.parametersEP.FoRT) .* (0.341.*Cajo.*exp(2.*Vmo.*obj.parametersEP.FoRT)-0.341.*obj.parametersEP.Cao) ./(exp(2.*Vmo.*obj.parametersEP.FoRT)-1);
        ibarca_sl = obj.parametersEP.pCa.*4.*(Vmo.*obj.parametersEP.Frdy.*obj.parametersEP.FoRT) .* (0.341.*Caslo.*exp(2.*Vmo.*obj.parametersEP.FoRT)-0.341.*obj.parametersEP.Cao) ./(exp(2.*Vmo.*obj.parametersEP.FoRT)-1);
        ibark = obj.parametersEP.pK.*(Vmo.*obj.parametersEP.Frdy.*obj.parametersEP.FoRT).*(0.75.*Kio.*exp(Vmo.*obj.parametersEP.FoRT)-0.75.*obj.parametersEP.Ko) ./(exp(Vmo.*obj.parametersEP.FoRT)-1);
        ibarna_j = obj.parametersEP.pNa.*(Vmo.*obj.parametersEP.Frdy.*obj.parametersEP.FoRT) .*(0.75.*Najo.*exp(Vmo.*obj.parametersEP.FoRT)-0.75.*obj.parametersEP.Nao)  ./(exp(Vmo.*obj.parametersEP.FoRT)-1);
        ibarna_sl = obj.parametersEP.pNa.*(Vmo.*obj.parametersEP.Frdy.*obj.parametersEP.FoRT) .*(0.75.*Naslo.*exp(Vmo.*obj.parametersEP.FoRT)-0.75.*obj.parametersEP.Nao)  ./(exp(Vmo.*obj.parametersEP.FoRT)-1);
        I_Ca_junc = (obj.parametersEP.Fjunc_CaL.*ibarca_j.*do.*fo.*((1-fcaBjo)+fcaCaj).*obj.parametersEP.Q10CaL.^obj.parametersEP.Qpow).*0.45.*1;
        I_Ca_sl = (obj.parametersEP.Fsl_CaL.*ibarca_sl.*do.*fo.*((1-fcaBslo)+fcaCaMSL).*obj.parametersEP.Q10CaL.^obj.parametersEP.Qpow).*0.45.*1;
        I_Ca = I_Ca_junc+I_Ca_sl;
        I_CaK = (ibark.*do.*fo.*(obj.parametersEP.Fjunc_CaL.*(fcaCaj+(1-fcaBjo))+obj.parametersEP.Fsl_CaL.*(fcaCaMSL+(1-fcaBslo))).*obj.parametersEP.Q10CaL.^obj.parametersEP.Qpow).*0.45.*1;
        I_CaNa_junc = (obj.parametersEP.Fjunc_CaL.*ibarna_j.*do.*fo.*((1-fcaBjo)+fcaCaj).*obj.parametersEP.Q10CaL.^obj.parametersEP.Qpow).*0.45.*1;
        I_CaNa_sl = (obj.parametersEP.Fsl_CaL.*ibarna_sl.*do.*fo.*((1-fcaBslo)+fcaCaMSL).*obj.parametersEP.Q10CaL.^obj.parametersEP.Qpow).*.45.*1;
        I_CaNa = I_CaNa_junc+I_CaNa_sl;
        I_Catot = I_Ca+I_CaK+I_CaNa;
        
        % I_ncx: Na./Ca Exchanger flux
        Ka_junc = 1./(1+(obj.parametersEP.Kdact./Cajo).^3);
        Ka_sl = 1./(1+(obj.parametersEP.Kdact./Caslo).^3);
        s1_junc = exp(obj.parametersEP.nu.*Vmo.*obj.parametersEP.FoRT).*Najo.^3.*obj.parametersEP.Cao;
        s1_sl = exp(obj.parametersEP.nu.*Vmo.*obj.parametersEP.FoRT).*Naslo.^3.*obj.parametersEP.Cao;
        s2_junc = exp((obj.parametersEP.nu-1).*Vmo.*obj.parametersEP.FoRT).*obj.parametersEP.Nao.^3.*Cajo;
        s3_junc = obj.parametersEP.KmCai.*obj.parametersEP.Nao.^3.*(1+(Najo./obj.parametersEP.KmNai).^3) + obj.parametersEP.KmNao.^3.*Cajo.*(1+Cajo./obj.parametersEP.KmCai)+obj.parametersEP.KmCao.*Najo.^3+Najo.^3.*obj.parametersEP.Cao+obj.parametersEP.Nao.^3.*Cajo;
        s2_sl = exp((obj.parametersEP.nu-1).*Vmo.*obj.parametersEP.FoRT).*obj.parametersEP.Nao.^3.*Caslo;
        s3_sl = obj.parametersEP.KmCai.*obj.parametersEP.Nao.^3.*(1+(Naslo./obj.parametersEP.KmNai).^3) + obj.parametersEP.KmNao.^3.*Caslo.*(1+Caslo./obj.parametersEP.KmCai)+obj.parametersEP.KmCao.*Naslo.^3+Naslo.^3.*obj.parametersEP.Cao+obj.parametersEP.Nao.^3.*Caslo;
        I_ncx_junc = obj.parametersEP.Fjunc.*obj.parametersEP.IbarNCX.*obj.parametersEP.Q10NCX.^obj.parametersEP.Qpow.*Ka_junc.*(s1_junc-s2_junc)./s3_junc./(1+obj.parametersEP.ksat.*exp((obj.parametersEP.nu-1).*Vmo.*obj.parametersEP.FoRT));
        I_ncx_sl = obj.parametersEP.Fsl.*obj.parametersEP.IbarNCX.*obj.parametersEP.Q10NCX.^obj.parametersEP.Qpow.*Ka_sl.*(s1_sl-s2_sl)./s3_sl./(1+obj.parametersEP.ksat.*exp((obj.parametersEP.nu-1).*Vmo.*obj.parametersEP.FoRT));
        I_ncx = I_ncx_junc+I_ncx_sl;
        
        % I_obj.parametersEP.pCa: Sarcolemmal Ca Pump Current
        I_pCa_junc = obj.parametersEP.Fjunc.*obj.parametersEP.Q10SLCaP.^obj.parametersEP.Qpow.*obj.parametersEP.IbarSLCaP.*Cajo.^1.6./(obj.parametersEP.KmPCa.^1.6+Cajo.^1.6);
        I_pCa_sl = obj.parametersEP.Fsl.*obj.parametersEP.Q10SLCaP.^obj.parametersEP.Qpow.*obj.parametersEP.IbarSLCaP.*Caslo.^1.6./(obj.parametersEP.KmPCa.^1.6+Caslo.^1.6);
        I_pCa = I_pCa_junc+I_pCa_sl;
        
        % I_cabk: Ca Background Current
        I_cabk_junc = obj.parametersEP.Fjunc.*obj.parametersEP.GCaB.*(Vmo-obj.parametersEP.eca_junc);
        I_cabk_sl = obj.parametersEP.Fsl.*obj.parametersEP.GCaB.*(Vmo-obj.parametersEP.eca_sl);
        I_cabk = I_cabk_junc+I_cabk_sl;
        
        %% SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
        MaxSR = 15; MinSR = 1;
        kCaSR = MaxSR - (MaxSR-MinSR)./(1+(obj.parametersEP.ec50SR./Ca_sro).^2.5);
        obj.parametersEP.KoSRCa = obj.parametersEP.koCa./kCaSR;
        kiSRCa = obj.parametersEP.kiCa.*kCaSR;
        RI = 1-RyRro-RyRoo-RyRio;
        dRyRro = (obj.parametersEP.kim.*RI-kiSRCa.*Cajo.*RyRro)-(obj.parametersEP.KoSRCa.*Cajo.^2.*RyRro-obj.parametersEP.kom.*RyRoo);   % R
        dRyRoo = (obj.parametersEP.KoSRCa.*Cajo.^2.*RyRro-obj.parametersEP.kom.*RyRoo)-(kiSRCa.*Cajo.*RyRoo-obj.parametersEP.kim.*RyRio);% O
        dRyRio = (kiSRCa.*Cajo.*RyRoo-obj.parametersEP.kim.*RyRio)-(obj.parametersEP.kom.*RyRio-obj.parametersEP.KoSRCa.*Cajo.^2.*RI);   % I
        
        
        % %% Traveling wave
        % if hs_counter == round(obj.segments./2)
        %     if t>=2 && t<=2.5
        %         dRyRoo = 1;%O
        %     end
        % end
        
        %%
        J_SRCarel = obj.parametersEP.ks.*RyRoo.*(Ca_sro-Cajo);          % [mM./ms]
        
        J_serca = obj.parametersEP.Q10SRCaP.^obj.parametersEP.Qpow.*obj.parametersEP.Vmax_SRCaP.*((Caio./obj.parametersEP.Kmf).^obj.parametersEP.hillSRCaP-(Ca_sro./obj.parametersEP.Kmr).^obj.parametersEP.hillSRCaP)...
            ./(1+(Caio./obj.parametersEP.Kmf).^obj.parametersEP.hillSRCaP+(Ca_sro./obj.parametersEP.Kmr).^obj.parametersEP.hillSRCaP);
        J_SRleak = 5.348e-6.*(Ca_sro-Cajo);           %   [mM./ms]
        
        %% Sodium and Calcium Buffering
        dNaBjo = obj.parametersEP.kon_na.*Najo.*(obj.parametersEP.Bmax_Naj-NaBjo)-obj.parametersEP.koff_na.*NaBjo;        % NaBj      [mM./ms]
        dNaBslo = obj.parametersEP.kon_na.*Naslo.*(obj.parametersEP.Bmax_Nasl-NaBslo)-obj.parametersEP.koff_na.*NaBslo;       % NaBsl     [mM./ms]
        
        % Cytosolic Ca Buffers
        dTnClo = obj.parametersEP.kon_tncl.*Caio.*(obj.parametersEP.Bmax_TnClow-TnClo)-obj.parametersEP.koff_tncl.*TnClo;            % TnCL      [mM./ms]
        dTnClo = obj.parametersEP.Bmax_TnClow.*dTnCTot;
        dTnCHco = obj.parametersEP.kon_tnchca.*Caio.*(obj.parametersEP.Bmax_TnChigh-TnCHco-TnCHmo)-obj.parametersEP.koff_tnchca.*TnCHco; % TnCHc     [mM./ms]
%         dTnCHco = obj.parametersEP.Bmax_TnChigh .* dTnC;
        dTnCHmo = obj.parametersEP.kon_tnchmg.*obj.parametersEP.Mgi.*(obj.parametersEP.Bmax_TnChigh-TnCHco-TnCHmo)-obj.parametersEP.koff_tnchmg.*TnCHmo;   % TnCHm     [mM./ms]
        dCaMo = obj.parametersEP.kon_cam.*Caio.*(obj.parametersEP.Bmax_TnChigh-CaMo)-obj.parametersEP.koff_cam.*CaMo;                 % CaM       [mM./ms]
        dMyoco = obj.parametersEP.kon_myoca.*Caio.*(obj.parametersEP.Bmax_myosin-Myoco-Myomo)-obj.parametersEP.koff_myoca.*Myoco;    % Myosin_ca [mM./ms]
        dMyomo = obj.parametersEP.kon_myomg.*obj.parametersEP.Mgi.*(obj.parametersEP.Bmax_myosin-Myoco-Myomo)-obj.parametersEP.koff_myomg.*Myomo;      % Myosin_mg [mM./ms]
        dSRBo = obj.parametersEP.kon_sr.*Caio.*(obj.parametersEP.Bmax_SR-SRBo)-obj.parametersEP.koff_sr.*SRBo;                    % SRB       [mM./ms]
        J_CaB_cytosol = dTnClo+dTnCHco+dTnCHmo+dCaMo+dMyoco+dMyomo+dSRBo;
        
        % Junctional and SL Ca Buffers
        dSLLjo = obj.parametersEP.kon_sll.*Cajo.*(obj.parametersEP.Bmax_SLlowj-SLLjo)-obj.parametersEP.koff_sll.*SLLjo;       % SLLj      [mM./ms]
        dSLLslo = obj.parametersEP.kon_sll.*Caslo.*(obj.parametersEP.Bmax_SLlowsl-SLLslo)-obj.parametersEP.koff_sll.*SLLslo;      % SLLsl     [mM./ms]
        dSLHjo = obj.parametersEP.kon_slh.*Cajo.*(obj.parametersEP.Bmax_SLhighj-SLHjo)-obj.parametersEP.koff_slh.*SLHjo;      % SLHj      [mM./ms]
        dSLHslo = obj.parametersEP.kon_slh.*Caslo.*(obj.parametersEP.Bmax_SLhighsl-SLHslo)-obj.parametersEP.koff_slh.*SLHslo;     % SLHsl     [mM./ms]
        J_CaB_junction = dSLLjo+dSLHjo;
        J_CaB_sl = dSLLslo+dSLHslo;
        
        %% Ion concentrations
        % SR Ca Concentrations
        dCsqnbo = obj.parametersEP.kon_csqn.*Ca_sro.*(obj.parametersEP.Bmax_Csqn-Csqnbo)-obj.parametersEP.koff_csqn.*Csqnbo;       % Csqn      [mM./ms]
        % dCa_sro = J_serca-(J_SRleak.*Vmyo./obj.parametersEP.Vsr+J_SRCarel)-dCsqnbo;         % Ca_sr     [mM./ms] %Ratio 3 leak current
        
        % Sodium Concentrations
        I_Na_tot_junc = I_Na_junc+I_nabk_junc+3.*I_ncx_junc+3.*I_nak_junc+I_CaNa_junc;   % [uA./uF]
        I_Na_tot_sl = I_Na_sl+I_nabk_sl+3.*I_ncx_sl+3.*I_nak_sl+I_CaNa_sl;   % [uA./uF]
        
        dNajo = -I_Na_tot_junc.*obj.parametersEP.Cmem./(obj.parametersEP.Vjunc.*obj.parametersEP.Frdy)+obj.parametersEP.J_na_juncsl./(obj.parametersEP.Vjunc).*(Naslo-Najo)-dNaBjo;
        dNaslo = -I_Na_tot_sl.*obj.parametersEP.Cmem./(obj.parametersEP.Vsl.*obj.parametersEP.Frdy)+obj.parametersEP.J_na_juncsl./(obj.parametersEP.Vsl).*(Najo-Naslo)...
            +obj.parametersEP.J_na_slmyo./(obj.parametersEP.Vsl).*(Naio-Naslo)-dNaBslo;
        %dNajo = 0;
        %dNaslo = 0;
        dNaio = obj.parametersEP.J_na_slmyo./(obj.parametersEP.Vmyo).*(Naslo-Naio);             % [mM./msec]
        %dNajo=0;
        
        % Potassium Concentration
        I_K_tot = I_to+I_kr+I_ks+I_ki-2.*I_nak+I_CaK+I_kp;     % [uA./uF]
        % dKio = 0; %-I_K_tot.*Cmem./(obj.parametersEP.Vmyo.*obj.parametersEP.Frdy);           % [mM./msec]
        dKio =0; % -I_K_tot.*Cmem./(obj.parametersEP.Vmyo.*obj.parametersEP.Frdy);
        
        % Calcium Concentrations
        I_Ca_tot_junc = I_Ca_junc+I_cabk_junc+I_pCa_junc-2.*I_ncx_junc;                   % [uA./uF]
        I_Ca_tot_sl = I_Ca_sl+I_cabk_sl+I_pCa_sl-2.*I_ncx_sl;            % [uA./uF]
        
        %% For the first, leftmost 1 (d=1) connected to membrane 1
        J_diff_CytSL = obj.parametersEP.J_ca_slmyo*(Caio-Caslo);
        J_diff_juncSL = obj.parametersEP.J_ca_juncsl.*(Cajo-Caslo);
        
        dCajo = -I_Ca_tot_junc.*obj.parametersEP.Cmem./(obj.parametersEP.Vjunc.*2.*obj.parametersEP.Frdy)...
            - J_CaB_junction+(J_SRCarel).*obj.parametersEP.Vsr./obj.parametersEP.Vjunc...
            + J_SRleak.*obj.parametersEP.Vmyo./obj.parametersEP.Vjunc...
            - J_diff_juncSL./obj.parametersEP.Vjunc;
        
        dCaslo = -I_Ca_tot_sl.*obj.parametersEP.Cmem./(obj.parametersEP.Vsl.*2.*obj.parametersEP.Frdy)...
            - J_CaB_sl + J_diff_juncSL./obj.parametersEP.Vsl + J_diff_CytSL./obj.parametersEP.Vsl;
        
        dCaio = -J_serca.*obj.parametersEP.Vsr./obj.parametersEP.Vmyo...
            - J_CaB_cytosol - J_diff_CytSL./obj.parametersEP.Vmyo;
        
        dCa_sro = J_serca...
            - (J_SRleak.*obj.parametersEP.Vmyo./obj.parametersEP.Vsr+J_SRCarel)...
            - dCsqnbo;
        
        
        % %% SAC
        % %%
        % % I_NS (cation-nonselective SAC) current
        % E_ns = -10; %mV
        % G_K0 = 1./10; %zwischen 0 und 1./10
        % G_ns = 0.08;%0.08;
        % I_ns = G_ns .* (Vmo - E_ns);
        %
        % %SAC_K (K+ selective SAC) current
        % I_K0 = G_K0 ./ (1 + exp((19.05 - Vmo)./29.98));
        %
        % %SAC current
        % I_SAC = (I_ns + I_K0) .* (strainfunc);
        %
        %
        % I_SAC = [I_SAC,I_SAC];
        % I_SAC = zeros(size([strainfunc,strainfunc]));
        % % if (nargin == 6) & strcmp(runType,'currents')
        % %     I_SAC = [I_SAC(1:segments);I_SAC(segments+1:end)]';
        % % end
        
        I_SAC = 0;
        %% Simulation type
        protocol = 'pace1';
        
        switch lower(protocol)
            case {'none',''},
                I_app = 0;
            case 'pace1',        % pace w/ current injection at rate 'rate'
                rate = 1e-3;
                if mod(time_step.*1000+975,1/rate) <= 5
                    I_app = 9.5;
                else
                    I_app = 0.0;
                end
                %          I_app = 0.0;
            case 'pace2',        % pace w/ current injection at rate 'rate'
                factor = 2;
                rate = factor*1e-3;
                if (mod(t+900,1/rate) <= 5) & ((t > 5000) & (t < 10000))
                    I_app = 12.0;
                elseif (mod(t+900,1/rate*factor*2) <= 5)  & ((t <= 5000) | (t >= 10000))
                    I_app = 12.0;
                else
                    I_app = 0;
                end
                I_app = 0;
            case 'pace3',
                rate = 40e-3;
                if (t > 1000) & (t < 2000) & (mod(t+900,1/rate) <= 5)
                    I_app = 10;
                elseif (t > 2000) & (mod(t+900,1/rate*10) <= 5)
                    I_app = 10;
                else
                    I_app = 0;
                end
            case 'vclamp',
                V_hold = -90;
                V_test = 0;
                if (t > 100 & t < 300)
                    V_clamp = V_test;
                else
                    V_clamp = V_hold;
                end
                R_clamp = 0.02;
                I_app = (V_clamp-Vmo)/R_clamp;
            case 'fig5',
                rate = 0.5e-3;
                %if t<=60000
                %   V_clamp=-90;
                %else
                if mod(t,1/rate)<=200
                    V_clamp = -20;
                else
                    V_clamp = -90;
                end
                %end
                
                R_clamp = 0.02;
                I_app = (V_clamp-Vmo)/R_clamp;
                
        end
        
        %% Membrane Potential
        I_Na_tot = I_Na_tot_junc + I_Na_tot_sl;          % [uA./uF]
        I_Cl_tot = I_ClCa+I_Clbk;                        % [uA./uF]
        I_Ca_tot = I_Ca_tot_junc+I_Ca_tot_sl;
        I_tot = I_Na_tot+I_Cl_tot+I_Ca_tot+I_K_tot+I_SAC;
        %dVmo = -(I_Ca_tot+I_K_tot+I_Na_tot-I_app);
        dVmo = -(I_tot-I_app);
        vmax = dVmo;
        
        %% output vector
        ydot(1)  =  dVmo;
        
        %membrane 1
        ydot(2)  =  dmo;
        ydot(3)  =  dho;
        ydot(4)  =  djo;
        ydot(5)  =  ddo;
        ydot(6)  =  dfo;
        ydot(7)  =  dfcaBjo;
        ydot(8)  =  dfcaBslo;
        ydot(9)  =  dxtoso;
        ydot(10)  =  dytoso;
        ydot(11)  =  dxtofo;
        ydot(12)  =  dytofo;
        ydot(13)  =  dxkro;
        ydot(14)  =  dxkso;
        ydot(15)  =  dRyRro;
        ydot(16)  =  dRyRoo;
        ydot(17)  =  dRyRio;
        ydot(18)  =  dNaBjo;
        ydot(19)  =  dNaBslo;
        ydot(20)  =  dTnClo;
        ydot(21)  =  dTnCHco;
        ydot(22)  =  dTnCHmo;
        ydot(23)  =  dCaMo;
        ydot(24)  =  dMyoco;
        ydot(25)  =  dMyomo;
        ydot(26)  =  dSRBo;
        ydot(27)  =  dSLLjo;
        ydot(28)  =  dSLLslo;
        ydot(29)  =  dSLHjo;
        ydot(30)  =  dSLHslo;
        ydot(31)  =  dCsqnbo;
        ydot(32)  =  dCa_sro;
        ydot(33)  =  dNajo;
        ydot(34)  =  dNaslo;
        ydot(35)  =  dNaio;
        ydot(36)  =  dKio;
        ydot(37)  =  dCajo;
        ydot(38)  =  dCaslo;
        ydot(39)  =  dCaio;
        ydot(40)  =  drtoso;
        ydot(41)  =  dTnCL;
        ydot(42)  =  dTnCH;
        ydot(43)  =  dM3EP;
        
        ydot = ydot.*1000;
        
        if isreal(yEP) == 0
            disp('halt');
        end
        
        if isreal(ydot) == 0
            disp('halt');
        end
        
        % ----- END EC COUPLING MODEL ---------------
        % adjust output depending on the function call
        
        if (nargin == 2)
            dy = [dy;ydot];
        elseif (nargin == 3) & strcmp(runType,'ydot')
            dy = ydot;
        elseif (nargin == 3) & strcmp(runType,'rates')
            dy = r;
        elseif (nargin == 3) & strcmp(runType,'currents')
            %currents = [I_Na I_nabk I_nak I_kr I_ks I_kp I_tos I_tof I_ki I_ClCa I_Clbk I_Catot I_ncx I_pCa I_cabk J_serca.*obj.parametersEP.Vmyo./obj.parametersEP.Vsr];
            %currents = [I_Na I_tof I_tos I_kr I_ks I_ClCa I_Catot J_SRCarel.*obj.parametersEP.Vsr./obj.parametersEP.Vmyo J_SRleak RI I_ncx];
            currents= [I_ncx I_Na I_kr I_ks I_Catot I_Ca];
            dy = currents;
        end
        
%         %put everything together
%         dy = [dy;output];
        
    end
    
    %% Calculate timecourse for currents and other intermediates
    function currents = calcCurrents(t,y)
    % After running a simulation, feed the time vector and state variables into
    % this function to compute ionic currents, etc.
    % currents: [I_Na,I_Catot];
    currents=[];
    for i=1:length(t)
        if ceil(i./10)==i./10
%             disp(['t = ',num2str(ceil(t(i)))]);
        end
        currents=[currents;derivs(t(i),y(i,:)','currents')];
    end
    end % end calcCurrents

    function r = constrain_rate(r)
        if (r<0)
            r=0;
        end
        if (r>obj.parameters.max_rate)
            r=obj.parameters.max_rate;
        end
    end
end
