function impose_force_balance(obj)
% Function updates the length of each half-sarcomere and
% the series component to maintain force balance

% Create a vector p which has the lengths of each half-sarcomere
% followed by the muscle force
for hs_counter = 1:obj.no_of_half_sarcomeres
    p(hs_counter) = obj.hs(hs_counter).hs_length;
end
p(obj.no_of_half_sarcomeres+1) = obj.muscle_force;

% Now try and impose force balance
opt = optimoptions('fsolve','Display','none');
new_p = fsolve(@muscle_system,p,opt);

% Unpack the new_p array and implement
for hs_counter = 1:obj.no_of_half_sarcomeres
    dhsl = new_p(hs_counter) - obj.hs(hs_counter).hs_length;
    obj.hs(hs_counter).move_cb_distribution(dhsl);
    obj.hs(hs_counter).hs_length = new_p(hs_counter);
end

obj.series_extension = return_series_extension(obj,new_p(end));
obj.muscle_force = new_p(end);


%update lattice spacing
for hs_counter = 1:obj.no_of_half_sarcomeres
    obj.M3(hs_counter) = sum(obj.hs(hs_counter).myofilaments.y(2+(1:obj.hs(hs_counter).myofilaments.no_of_x_bins)));
    
    l_r_xksi(hs_counter) = obj.LatticeSpacing.l_r0 + obj.LatticeSpacing.l_r1.*(obj.LatticeSpacing.hs_length_rest(hs_counter)-obj.hs(hs_counter).hs_length);
    if hs_counter == 1
        V_lattice(hs_counter) = obj.LatticeSpacing.k_mz .*((obj.LatticeSpacing.lattice_0 - obj.lattice(1)) + (obj.lattice(2) - obj.lattice(1)));
    elseif hs_counter == obj.no_of_half_sarcomeres
        V_lattice(hs_counter) = obj.LatticeSpacing.k_mz .*((obj.lattice(obj.no_of_half_sarcomeres-1) - obj.lattice(obj.no_of_half_sarcomeres)) + (obj.LatticeSpacing.lattice_end - obj.lattice(obj.no_of_half_sarcomeres)));
    else
        V_lattice(hs_counter) = obj.LatticeSpacing.k_mz .*((obj.lattice(hs_counter-1) - obj.lattice(hs_counter)) + (obj.lattice(hs_counter + 1) - obj.lattice(hs_counter)));
    end
    obj.dlattice(hs_counter) =  ((obj.LatticeSpacing.k_r .* (l_r_xksi(hs_counter) - obj.lattice(hs_counter)) + ...
            obj.LatticeSpacing.k_m.*obj.M3(hs_counter).*(obj.LatticeSpacing.hs_length_rest(hs_counter)-obj.hs(hs_counter).hs_length).*(obj.LatticeSpacing.l_m-...
            obj.lattice(hs_counter))./obj.LatticeSpacing.s_0).*obj.LatticeSpacing.eta_d + V_lattice(hs_counter).*obj.LatticeSpacing.eta_d);
end

obj.lattice = obj.lattice + obj.dlattice;

    % Nested function
    function x = muscle_system(p)
        x=zeros(numel(p),1);
        for i=1:obj.no_of_half_sarcomeres
            check_new_force(obj.hs(i),p(i));
            x(i) = obj.hs(i).check_force - p(end);
        end
        new_series_extension = obj.muscle_length - ...
            sum(p(1:obj.no_of_half_sarcomeres));
        x(end) = return_series_force(obj,new_series_extension) - ...
            p(end);
    end
end
