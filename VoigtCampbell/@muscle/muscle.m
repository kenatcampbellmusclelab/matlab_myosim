classdef muscle < handle
    
    properties
        % These are properties that can be accessed from outside the
        % muscle class
        
        series_k_linear = 1e4;
        muscle_length;
        muscle_force = 0;
        
        % A holder for an array of half_sarcomeres
        hs = half_sarcomere;
        output;
        
        %% Lattice Spacing
        LatticeSpacing = [];
        lattice;
        dlattice;
        M3;
    end
    
    properties (Constant)
        no_of_half_sarcomeres = 3;
    end
    properties (SetAccess = private)
        % These are properties that can only be accessed from within the
        % muscle class
        
        series_extension;
        
    end
    
    methods
        
        % Constructor
        function obj = muscle(varargin)
            
            % Set up muscle
            
            % Start by updating variables
            muscle_props = varargin{1}
            
            obj.series_k_linear = muscle_props.series_k_linear;
            
            % Now create half_sarcomeres, updating muscle length as we go
            obj.muscle_length = 0;
            for hs_counter = 1:obj.no_of_half_sarcomeres
                obj.hs(hs_counter) = half_sarcomere();
                obj.muscle_length = obj.muscle_length + ...
                    obj.hs(hs_counter).hs_length;
                obj.LatticeSpacing.hs_length_rest(hs_counter) = obj.hs(hs_counter).hs_length;
                obj.lattice(hs_counter) = 25.1;
                obj.dlattice(hs_counter) = 0;
                obj.M3(hs_counter) = 0;
            end
            
            %% Lattice Spacing Parameters
            obj.LatticeSpacing.l_r0 = 23;
            obj.LatticeSpacing.l_r1 = 2.53;
            obj.LatticeSpacing.lattice_0 = 25.3;
            obj.LatticeSpacing.lattice_end = 25.3;

            obj.LatticeSpacing.eta_d = 0.0005; 
            obj.LatticeSpacing.k_m = 1;
            obj.LatticeSpacing.s_0 = 0.01;
            obj.LatticeSpacing.l_m  = 23;
            obj.LatticeSpacing.k_r = 60;
            obj.LatticeSpacing.k_mz = 0.5;
            

            % Implement force balance
            impose_force_balance(obj);

        end
        
        % Other methods
        impose_force_balance(obj);
        series_extension = return_series_extension(obj,muscle_force);
        series_force = return_series_force(obj,series_extension);
        implement_time_step(obj,time_step,delta_ml);
        
        Voigt(obj,time_step)
    end
end
            
        